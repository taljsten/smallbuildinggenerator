﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingData 
{
    private List<Vector3> floorVertices;
    private List<Vector2> floorUV;
    private List<Triangle> floorTriangles;

    private List<Vector3> ceilingVertices;
    private List<Vector2> ceilingUV;
    private List<Triangle> ceilingTriangles;

    public List<MeshData> InnerWallsMeshData { get; set; }
    public List<MeshData> FacadeMeshData { get; set; }
    public List<MeshData> WindowsMeshData { get; set; }
    public List<MeshData> DoorFramesMeshData { get; set; }
    public List<MeshData> DoorPanelsMeshData { get; set; }
    public MeshData FloorData { get; set; }
    public MeshData CeilingData { get; set; }
    public MeshData RoofTopData { get; set; }
    public MeshData RoofSlantData { get; set; }

    public BuildingData()
    {
        floorVertices = new List<Vector3>();
        floorUV = new List<Vector2>();
        floorTriangles = new List<Triangle>();

        ceilingVertices = new List<Vector3>();
        ceilingUV = new List<Vector2>();
        ceilingTriangles = new List<Triangle>();

        InnerWallsMeshData = new List<MeshData>();
        FacadeMeshData = new List<MeshData>();
        
    }

    public void Add(BaseLayout baseLayout)
    {
        AddBase(baseLayout.Polygon);
        AddBase(baseLayout.Subdivision);
        FloorData = new MeshData()
        {
            Vertices = floorVertices.ToArray(),
            Triangles = floorTriangles.ToArray(),
            UV = floorUV.ToArray(),
            MaterialId = 0
        };
    }

    public void Add(CeilingLayout ceilingLayout)
    {
        AddCeiling(ceilingLayout.Polygon);
        AddCeiling(ceilingLayout.Subdivision);
        CeilingData = new MeshData()
        {
            Vertices = ceilingVertices.ToArray(),
            Triangles = ceilingTriangles.ToArray(),
            UV = ceilingUV.ToArray(),
            MaterialId = 0
        };
    }

    public void Add(FacadeLayout facadeLayout)
    {
        //Debug.Log(facadeLayout.Wall.GetEdges().Length);
        foreach (Edge2D edge in facadeLayout.InsideWall.GetEdges())
        {
            // One side
            MeshData[] walls01 = FeatureWall(edge, 0, true, BuildRules.InteriorRuleSet);
            FacadeMeshData.AddRange(walls01);
            
        }

        foreach (Edge2D edge in facadeLayout.OutsideWall.GetEdges())
        {
            
            // Other side
            MeshData[] walls02 = FeatureWall(edge, 0, false, BuildRules.FacadeRuleSet);
            FacadeMeshData.AddRange(walls02);
        }

    }

    public void Add(InteriorLayout interiorLayout)
    {        
        foreach (Polygon2D wall in interiorLayout.Walls)
        {             
            foreach (Edge2D edge in wall.GetEdges())
            {
                // One side
                MeshData[] walls01 = FeatureWall(edge, BuildRules.InnerWallThickness / 2, true, BuildRules.InteriorRuleSet);
                InnerWallsMeshData.AddRange(walls01);
                // Other side
                MeshData[] walls02 = FeatureWall(edge, -BuildRules.InnerWallThickness / 2, false, BuildRules.InteriorRuleSet);
                InnerWallsMeshData.AddRange(walls02);
            }
        }

    }

    public void Add(RoofLayout roofLayout, float[] angles)
    {
       
        AddRoofTop(roofLayout, BuildRules.RoofRuleSet);
        AddRoofSlant(roofLayout, BuildRules.RoofRuleSet, angles);
    }

    public void Add(DetailsLayout detailsLayout)
    {
        WindowsMeshData = detailsLayout.GetWindowsMeshData();
        DoorFramesMeshData = detailsLayout.GetDoorFramesMeshData();
        DoorPanelsMeshData = detailsLayout.GetDoorPanelsMeshData();
    }

    private void AddRoofSlant(RoofLayout roofLayout, RuleSet ruleset, float[] angles)
    {
        Vector3[] verts = roofLayout.SlantSubdivision.Vertices.ToArray();

        // divide UVs in 4 by 4 verts
        Vector2[] uv = new Vector2[verts.Length];
        //float hypotenusa = Mathf.Sqrt(2) * roofLayout.Slant;

        
        int length = uv.Length / 4;
        for (int i = 1; i < length*2; i+=2)
        {
            int i0 = i;
            int i1 = i + 1;
            int i2 = i + length * 2;
            int i3 = i + length * 2 + 1;
            if (i==length*2-1)
            {
                i1 = 0;
                i3 = length*2;
            }

            Vector2 v0 = new Vector2(verts[i0].x, verts[i0].z);
            Vector2 v1 = new Vector2(verts[i1].x, verts[i1].z);

            Vector2 direction = (v1 - v0).normalized;
            float angle = direction.RealAngleTo(Vector2.right);



            uv[i0] = (new Vector2(verts[i0].x, verts[i0].z) / ruleset.MaterialScale).RotateRadians(angle);
            uv[i1] = (new Vector2(verts[i1].x, verts[i1].z) / ruleset.MaterialScale).RotateRadians(angle);
            uv[i2] = (new Vector2(verts[i2].x, verts[i2].z) / ruleset.MaterialScale).RotateRadians(angle);
            uv[i3] = (new Vector2(verts[i3].x, verts[i3].z) / ruleset.MaterialScale).RotateRadians(angle);


        }
        RoofSlantData = new MeshData()
        {
            Vertices = roofLayout.SlantSubdivision.Vertices.ToArray(),
            Triangles = roofLayout.SlantSubdivision.Triangles.ToArray(),
            MaterialId = ruleset.MaterialId,
            UV = uv
           
        };
        
    }

    private void AddRoofTop(RoofLayout roofLayout, RuleSet ruleSet)
    {
        Vector3[] topVerts = new Vector3[roofLayout.Offset.GetVertices().Length];
        float height = ruleSet.WallHeight + roofLayout.Slant;
        for (int i = 0; i < topVerts.Length; i++)
        {
            topVerts[i] = new Vector3(roofLayout.Offset.VertexAt(i).position.x,
                                      height,
                                      roofLayout.Offset.VertexAt(i).position.y);
        }

        Vector2[] uv = roofLayout.Offset.VerticesToVectors();
        for (int i = 0; i < uv.Length; i++)
        {
            uv[i] /= ruleSet.MaterialScale;
        }


        RoofTopData = new MeshData()
        {
            Vertices = topVerts,
            Triangles = roofLayout.TopSubdivision.Triangles.ToArray(),
            MaterialId = ruleSet.MaterialId,
            UV = uv
        };
    }

    private MeshData[] FeatureWall(Edge2D edge, float offset, bool reversed, RuleSet ruleset)
    {
        List<MeshData> meshData = new List<MeshData>();
        
        
        foreach (Feature feature in edge.GetFeatures())
        {
            switch (feature.Type)
            {
                case FeatureType.Door:
                    meshData.Add(CreateDoorSection(edge, feature, offset, reversed, ruleset));                    
                    break;
                case FeatureType.Window:
                    meshData.Add(CreateWindowSection(edge, feature, offset, reversed, ruleset));
                    break;
                case FeatureType.Wall:
                    meshData.Add(CreateWallSection(edge, feature, offset, reversed, ruleset));
                    break;
                default:
                    meshData.Add(CreateWallSection(edge, feature, offset, reversed, ruleset));
                    break;
            }
        }

        return meshData.ToArray();
    }

    private MeshData CreateDoorSection(Edge2D edge, Feature feature, float offset, bool reversed, RuleSet ruleset)
    {
        Vector2 orthoDir = edge.Direction.RotateRadians(90 * Mathf.Deg2Rad);
        Vector3 orthoDirVector3 = new Vector3(orthoDir.x, 0, orthoDir.y);

        // 2---3
        // |  /|       
        // |/  |
        // 0---1
        Vector3[] v = new Vector3[4];
        v[0] = new Vector3(edge.Value(feature.StartPos).x, ruleset.DoorSize.y, edge.Value(feature.StartPos).y ) + orthoDirVector3 * offset;
        v[1] = new Vector3(edge.Value(feature.EndPos).x, ruleset.DoorSize.y, edge.Value(feature.EndPos).y ) + orthoDirVector3 * offset;
        v[2] = v[0] + Vector3.up * (ruleset.WallHeight - ruleset.DoorSize.y);
        v[3] = v[1] + Vector3.up * (ruleset.WallHeight - ruleset.DoorSize.y);

        Triangle[] t = new Triangle[2];
        if (reversed)
        {
            t[0] = new Triangle(0, 1, 3);
            t[1] = new Triangle(0, 3, 2);
        }
        else
        {
            t[0] = new Triangle(0, 3, 1);
            t[1] = new Triangle(0, 2, 3);
        }


        Vector2[] uv = new Vector2[4];

        float ratio = edge.Length / ruleset.WallHeight;
        //float h = BuildRules.InnerWallsTrimHeight * BuildRules.InnerWallsMaterialScale;
        //float w = ratio * h * feature.EndPos;
        float trimScale = ruleset.TrimHeight / ruleset.WallHeight;
        float scale = /*BuildRules.InnerWallsTrimHeight*/ trimScale * ruleset.MaterialScale;

        uv[0] = new Vector2(feature.StartPos * edge.Length * scale, ruleset.TrimPos + ruleset.DoorSize.y * scale);
        uv[1] = new Vector2(feature.EndPos * edge.Length * scale, ruleset.TrimPos + ruleset.DoorSize.y * scale);
        uv[2] = uv[0] + new Vector2(0, ruleset.WallHeight - ruleset.DoorSize.y) * scale;
        uv[3] = uv[1] + new Vector2(0, ruleset.WallHeight - ruleset.DoorSize.y) * scale;

        return new MeshData()
        {
            Vertices = v,
            Triangles = t,
            UV = uv,
            MaterialId = ruleset.MaterialId
        };
    }
   
    private MeshData CreateWindowSection(Edge2D edge, Feature feature, float offset, bool reversed, RuleSet ruleset)
    {
        Vector2 orthoDir = edge.Direction.RotateRadians(90 * Mathf.Deg2Rad);
        Vector3 orthoDirVector3 = new Vector3(orthoDir.x, 0, orthoDir.y);

        // 6---7
        // |  /|       
        // |/  |
        // 4---5
        //
        //
        // 2---3
        // |  /|       
        // |/  |
        // 0---1

        Vector3[] v = new Vector3[8];

        v[0] = new Vector3(edge.Value(feature.StartPos).x, 0, edge.Value(feature.StartPos).y) + orthoDirVector3 * offset;
        v[1] = new Vector3(edge.Value(feature.EndPos).x, 0, edge.Value(feature.EndPos).y) + orthoDirVector3 * offset;
        v[2] = v[0] + Vector3.up * ruleset.WindowStartHeight;
        v[3] = v[1] + Vector3.up * ruleset.WindowStartHeight;

        v[4] = v[2] + Vector3.up * ruleset.WindowSize.y;
        v[5] = v[3] + Vector3.up * ruleset.WindowSize.y;
        v[6] = v[4] + Vector3.up * (ruleset.WallHeight - ruleset.WindowSize.y - ruleset.WindowStartHeight);
        v[7] = v[5] + Vector3.up * (ruleset.WallHeight - ruleset.WindowSize.y - ruleset.WindowStartHeight);

        Triangle[] t = new Triangle[4];
        if (reversed)
        {
            t[0] = new Triangle(0, 1, 3);
            t[1] = new Triangle(0, 3, 2);
            t[2] = new Triangle(4, 5, 7);
            t[3] = new Triangle(4, 7, 6);
        }
        else
        {
            t[0] = new Triangle(0, 3, 1);
            t[1] = new Triangle(0, 2, 3);
            t[2] = new Triangle(4, 7, 5);
            t[3] = new Triangle(4, 6, 7);
        }


        Vector2[] uv = new Vector2[8];
        
        //float ratio = edge.Length / ruleset.WallHeight;
        float trimScale = ruleset.TrimHeight / ruleset.WallHeight;
        float scale = /*BuildRules.InnerWallsTrimHeight*/ trimScale * ruleset.MaterialScale;

        uv[0] = new Vector2(feature.StartPos * edge.Length * scale, ruleset.TrimPos);
        uv[1] = new Vector2(feature.EndPos * edge.Length * scale, ruleset.TrimPos);
        uv[2] = uv[0] + new Vector2(0, ruleset.WindowStartHeight) * scale;
        uv[3] = uv[1] + new Vector2(0, ruleset.WindowStartHeight) * scale;

        uv[4] = uv[2] + new Vector2(0, ruleset.WindowSize.y) * scale;
        uv[5] = uv[3] + new Vector2(0, ruleset.WindowSize.y) * scale;
        uv[6] = uv[0] + new Vector2(0, ruleset.WallHeight) * scale;
        uv[7] = uv[1] + new Vector2(0, ruleset.WallHeight) * scale;

        return new MeshData()
        {
            Vertices = v,
            Triangles = t,
            UV = uv,
            MaterialId = ruleset.MaterialId
        };
    }

    private MeshData CreateWallSection(Edge2D edge, Feature feature, float offset, bool reversed, RuleSet ruleset)
    {
        Vector2 orthoDir = edge.Direction.RotateRadians(90 * Mathf.Deg2Rad);
        Vector3 orthoDirVector3 = new Vector3(orthoDir.x, 0, orthoDir.y);
        
        // 2---3
        // |  /|
        // | / |
        // |/  |
        // 0---1
        Vector3[] v = new Vector3[4];
        v[0] = new Vector3(edge.Value(feature.StartPos).x, 0, edge.Value(feature.StartPos).y) + orthoDirVector3 * offset;
        v[1] = new Vector3(edge.Value(feature.EndPos).x, 0, edge.Value(feature.EndPos).y) + orthoDirVector3 * offset;
        v[2] = v[0] + Vector3.up * ruleset.WallHeight;
        v[3] = v[1] + Vector3.up * ruleset.WallHeight;

        Triangle[] t = new Triangle[2];
        if (reversed)
        {
            t[0] = new Triangle(0, 1, 3);
            t[1] = new Triangle(0, 3, 2);
        }
        else
        {
            t[0] = new Triangle(0, 3, 1);
            t[1] = new Triangle(0, 2, 3);
        }

        
        Vector2[] uv = new Vector2[4];

        //float ratio = edge.Length / ruleset.WallHeight;        
        float trimScale = ruleset.TrimHeight / ruleset.WallHeight;
        float scale = /*BuildRules.InnerWallsTrimHeight*/ trimScale * ruleset.MaterialScale;
        
        uv[0] = new Vector2(feature.StartPos * edge.Length * scale, ruleset.TrimPos);
        uv[1] = new Vector2(feature.EndPos * edge.Length * scale, ruleset.TrimPos) ;
        uv[2] = uv[0] + new Vector2(0, ruleset.WallHeight) * scale;
        uv[3] = uv[1] + new Vector2(0, ruleset.WallHeight) * scale;

        return new MeshData()
        {
            Vertices = v,
            Triangles = t,
            UV = uv,
            MaterialId = ruleset.MaterialId

        };
    }   

    private MeshData DoorWall(Edge2D edge, float doorPosT, float offset, bool reversed)
    {
        if (doorPosT < 0)
            return PlainWall(edge, offset, reversed);

        Vector2 orthoDir = edge.Direction.RotateRadians(90 * Mathf.Deg2Rad);
        Vector3 orthoDirVector3 = new Vector3(orthoDir.x, 0, orthoDir.y);
        Vector3 dir = new Vector3(edge.Direction.x, 0, edge.Direction.y);
        float h = BuildRules.WallHeight;
        //float doorH = BuildRules.DoorHeight;
        //float doorW = BuildRules.DoorWidth;
        Vector2 doorsize = BuildRules.DoorSize;
        float uvScale = BuildRules.InnerWallsMaterialScale; // 1 / (BuildRules.InnerWallsMaterialScale * 25); // 1 / (innerWallsUvScale * 10f);
        float b = Vector2.Distance(edge.Point1.position, edge.Point2.position);
        float doorPos = Vector2.Distance(edge.Point1.position, edge.Value(doorPosT));
        float ratio = BuildRules.InnerWallsTrimHeight / BuildRules.WallHeight;
        //float ratio2 = BuildRules.InnerWallsTrimHeight / h;

        // 8---9--10--11
        // | / | / | / |
        // 4---5---6---7
        // | / |   | / |        
        // 0---1   2---3

        Vector3[] v = new Vector3[12];
        v[0] = new Vector3(edge.Point1.position.x, 0, edge.Point1.position.y) + orthoDirVector3 * offset;
        v[1] = v[0] + dir * (doorPos - doorsize.x/2);
        v[2] = v[0] + dir * (doorPos + doorsize.x/2);
        v[3] = new Vector3(edge.Point2.position.x, 0, edge.Point2.position.y) + orthoDirVector3 * offset;
        v[4] = v[0] + Vector3.up * doorsize.y;
        v[5] = v[1] + Vector3.up * doorsize.y;
        v[6] = v[2] + Vector3.up * doorsize.y;
        v[7] = v[3] + Vector3.up * doorsize.y;
        v[8] = v[0] + Vector3.up * h;
        v[9] = v[1] + Vector3.up * h;
        v[10] = v[2] + Vector3.up * h;
        v[11] = v[3] + Vector3.up * h;

        Triangle[] t = new Triangle[10];
        if (reversed)
        {
            t[0] = new Triangle(0, 1, 5);
            t[1] = new Triangle(0, 5, 4);
            t[2] = new Triangle(4, 5, 9);
            t[3] = new Triangle(4, 9, 8);
            t[4] = new Triangle(5, 6, 10);
            t[5] = new Triangle(5, 10, 9);
            t[6] = new Triangle(6, 7, 11);
            t[7] = new Triangle(6, 11, 10);
            t[8] = new Triangle(2, 3, 7);
            t[9] = new Triangle(2, 7, 6);
        }
        else
        {
            t[0] = new Triangle(0, 5, 1);
            t[1] = new Triangle(0, 4, 5);
            t[2] = new Triangle(4, 9, 5);
            t[3] = new Triangle(4, 8, 9);
            t[4] = new Triangle(5, 10, 6);
            t[5] = new Triangle(5, 9, 10);
            t[6] = new Triangle(6, 11, 7);
            t[7] = new Triangle(6, 10, 11);
            t[8] = new Triangle(2, 7, 3);
            t[9] = new Triangle(2, 6, 7);
        }

        Vector2[] uv = new Vector2[v.Length];

        uv[0] = new Vector2(0, BuildRules.InnerWallsTrimPos);
        uv[1] = uv[0] + new Vector2(doorPosT * edge.Length - BuildRules.DoorSize.x, 0) * uvScale * ratio;
        uv[2] = uv[1] + new Vector2(BuildRules.DoorSize.x, 0) * uvScale * ratio;
        uv[3] = uv[0] + new Vector2(b, 0) * uvScale * ratio;
        uv[4] = uv[0] + new Vector2(0, BuildRules.DoorSize.y) * uvScale * ratio;
        uv[5] = uv[1] + new Vector2(0, BuildRules.DoorSize.y) * uvScale * ratio;
        uv[6] = uv[2] + new Vector2(0, BuildRules.DoorSize.y) * uvScale * ratio;
        uv[7] = uv[3] + new Vector2(0, BuildRules.DoorSize.y) * uvScale * ratio;
        uv[8] = uv[0] + new Vector2(0, h) * uvScale * ratio;
        uv[9] = uv[1] + new Vector2(0, h) * uvScale * ratio;
        uv[10] = uv[2] + new Vector2(0, h) * uvScale * ratio;
        uv[11] = uv[3] + new Vector2(0, h) * uvScale * ratio;

        return new MeshData()
        {
            Vertices = v,
            Triangles = t,
            UV = uv
        };
    }

    private MeshData PlainWall(Edge2D edge, float offset, bool reversed)
    {
        Vector2 orthoDir = edge.Direction.RotateRadians(90 * Mathf.Deg2Rad);
        Vector3 orthoDirVector3 = new Vector3(orthoDir.x, 0, orthoDir.y);
        
        float doorH = BuildRules.DoorSize.y;
        float doorW = BuildRules.DoorSize.x;
        float uvScale = BuildRules.InnerWallsMaterialScale; // 1 / (innerWallsUvScale * 10f);
        float h = BuildRules.WallHeight;
        float b = Vector2.Distance(edge.Point1.position, edge.Point2.position);
        float ratio = b / h;

        // 2---3
        // |  /|
        // | / |
        // |/  |
        // 0---1
        Vector3[] v = new Vector3[4];
        v[0] = new Vector3(edge.Point1.position.x, 0, edge.Point1.position.y) + orthoDirVector3 * offset;
        v[1] = new Vector3(edge.Point2.position.x, 0, edge.Point2.position.y) + orthoDirVector3 * offset;
        v[2] = v[0] + Vector3.up * h;
        v[3] = v[1] + Vector3.up * h;


        //InnerWallsVertices.AddRange(v);
        Triangle[] t = new Triangle[2];
        if (reversed)
        {            
            t[0] = new Triangle(0, 1, 3);
            t[1] = new Triangle(0, 3, 2); 
        }
        else
        {            
            t[0] = new Triangle(0, 3, 1);
            t[1] = new Triangle(0, 2, 3);
        }

        
        Vector2[] uv = new Vector2[4];
        uv[0] = new Vector2(0, BuildRules.InnerWallsTrimPos);
        uv[1] = uv[0] + new Vector2(ratio * BuildRules.InnerWallsTrimHeight, 0) * uvScale;
        uv[2] = uv[0] + new Vector2(0, BuildRules.InnerWallsTrimHeight) * uvScale;
        uv[3] = uv[0] + new Vector2(ratio * BuildRules.InnerWallsTrimHeight, BuildRules.InnerWallsTrimHeight) * uvScale;

        return new MeshData()
        {
            Vertices = v,
            Triangles = t,
            UV = uv
        };
    }

    private void AddBase(Polygon2D buildingBase)
    {
        //float uvScale = 1 / (Mathf.Max(buildingBase.Size.x, buildingBase.Size.y));
        float uvScale = 1 / (BuildRules.FloorMaterialScale * 10f);

        foreach (Vector2 vertex2D in buildingBase.VerticesToVectors())
        {
            
            Vector3 vertex = new Vector3(vertex2D.x, 0, vertex2D.y);
            floorVertices.Add(vertex);
            floorUV.Add(vertex2D * uvScale);
        }
    }

    private void AddCeiling(Polygon2D ceiling)
    {
        //float uvScale = 1 / (Mathf.Max(buildingBase.Size.x, buildingBase.Size.y));
        float uvScale = 1 / (BuildRules.FloorMaterialScale * 10f);

        foreach (Vector2 vertex2D in ceiling.VerticesToVectors())
        {

            Vector3 vertex = new Vector3(vertex2D.x, 0, vertex2D.y);
            ceilingVertices.Add(vertex);
            ceilingUV.Add(vertex2D * uvScale);
        }
    }

    private void AddBase(PolygonSubdivision buildingBaseSubdiv)
    {
        foreach (Triangle triangle in buildingBaseSubdiv.Triangles)
        {            
            floorTriangles.Add(triangle);
        }
    }

    private void AddCeiling(PolygonSubdivision ceilingSubdivision)
    {
        foreach (Triangle triangle in ceilingSubdivision.Triangles)
        {
            ceilingTriangles.Add(triangle);
        }
    }
}