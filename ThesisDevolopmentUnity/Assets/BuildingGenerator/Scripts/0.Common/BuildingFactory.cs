﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class BuildingFactory
{
    public static void CreateDoorFeatures(Polygon2D[] walls, float minDistanceFromVertex)
    {
        //List<Door2D> doors = new List<Door2D>();
        List<Vertex2D> vertices = new List<Vertex2D>();
        List<Edge2D> edges = new List<Edge2D>();
        foreach (Polygon2D wall in walls)
        {
            vertices.AddRange(wall.GetVertices());
            vertices.AddRange(wall.Intersections.ToVertex2DArray());
            foreach (Edge2D edge in wall.GetEdges())
            {
                edges.Add(edge);
            }
        }

        foreach (Edge2D edge in edges)
        {
            float slide = -1;
            int tries = 100;
            while (slide < 0 && tries>0)
            {
                tries--;
                slide = UnityEngine.Random.Range(0.1f, 0.9f);
                foreach (Vertex2D vertex in vertices)
                {
                    if (Vector2.Distance(vertex.position, edge.Value(slide)) < minDistanceFromVertex)
                    {
                        slide = -1;
                        break;
                    }
                }
            }

            if (slide >= 0)
            {
                Vector2 size = new Vector2(edge.LocalToValue(BuildRules.DoorSize.x), edge.LocalToValue(BuildRules.DoorSize.y));

                edge.AddFeature(FeatureType.Door, slide, size);

            }

            edge.FillEmptyWithWalls();
        }

        //return doors.ToArray();
    }

    public static void CreateFacade(FacadeLayout facadeLayout, float minDistanceFromVertex, int maxwindows)
    {
        Edge2D[] insideEdges = facadeLayout.InsideWall.GetEdges();
        Edge2D[] outsideEdges = facadeLayout.OutsideWall.GetEdges();       
        List<Vertex2D> vertices = new List<Vertex2D>();
        vertices.AddRange(facadeLayout.InsideWall.GetVertices());
        vertices.AddRange(facadeLayout.InsideWall.Intersections.ToVertex2DArray());

        List<Edge2D> orthoEdgesTroughWindows = new List<Edge2D>();
        List<Edge2D> orthoEdgesTroughDoors = new List<Edge2D>();

        int numberOfDoors = 1;
        foreach (Edge2D edge in insideEdges)
        {

            //edge.AddFeature(FeatureType.Window, 0.3f, size);
            //edge.AddFeature(FeatureType.Window, 0.6f, size);
            Vector2 windowSize = new Vector2(edge.LocalToValue(BuildRules.WindowsSize.x), edge.LocalToValue(BuildRules.WindowsSize.y));
            Vector2 doorSize = new Vector2(edge.LocalToValue(BuildRules.DoorSize.x), edge.LocalToValue(BuildRules.DoorSize.y));


            // Inside
            for (int i = 0; i < maxwindows; i++)
            {
                
                float slide = -1;
                int tries = 100;
                while (slide < 0 && tries > 0)
                {
                    tries--;
                    slide = UnityEngine.Random.Range(0.1f, 0.9f);
                    foreach (Vertex2D vertex in vertices)
                    {
                        if (Vector2.Distance(vertex.position, edge.Value(slide)) < minDistanceFromVertex)
                        {
                            slide = -1;
                            break;
                        }
                    }
                }

                if (slide >= 0)
                {
                    if (numberOfDoors==0)
                    {
                        edge.AddFeature(FeatureType.Window, slide, windowSize);

                        // So that new windows do not end up on top of this
                        vertices.Add(new Vertex2D(edge.Value(slide)));

                        // For use in outer walls below
                        Vector2 orthodir = edge.OrthoDirection;
                        Vector2 start = edge.Value(slide);
                        Edge2D orthoEdge = new Edge2D(
                            new Vertex2D(start - orthodir * BuildRules.FacadeRuleSet.WallThickness * 1.1f),
                            new Vertex2D(start + orthodir * BuildRules.FacadeRuleSet.WallThickness * 1.1f), EdgeType.External);

                        orthoEdgesTroughWindows.Add(orthoEdge); 
                    }
                    else
                    {
                        edge.AddFeature(FeatureType.Door, slide, doorSize);

                        // So that new windows do not end up on top of this
                        vertices.Add(new Vertex2D(edge.Value(slide)));

                        // For use in outer walls below
                        Vector2 orthodir = edge.OrthoDirection;
                        Vector2 start = edge.Value(slide);
                        Edge2D orthoEdge = new Edge2D(
                            new Vertex2D(start - orthodir * BuildRules.FacadeRuleSet.WallThickness * 1.1f),
                            new Vertex2D(start + orthodir * BuildRules.FacadeRuleSet.WallThickness * 1.1f), EdgeType.External);

                        orthoEdgesTroughDoors.Add(orthoEdge);
                        numberOfDoors--;
                    }

                } 
            }

            edge.FillEmptyWithWalls();
            
        }

        
        // Outside
        for (int i = 0; i < outsideEdges.Length; i++)
        {
            Edge2D edge = outsideEdges[i];
            Vector2 size = new Vector2(edge.LocalToValue(BuildRules.WindowsSize.x), edge.LocalToValue(BuildRules.WindowsSize.y));

            foreach (Edge2D e in orthoEdgesTroughWindows)
            {
                if (e.Intersects(edge, out float t1, out float t2))
                {
                    edge.AddFeature(FeatureType.Window, t2, size);
                }
            }

            size = new Vector2(edge.LocalToValue(BuildRules.DoorSize.x), edge.LocalToValue(BuildRules.DoorSize.y));
            foreach (Edge2D e in orthoEdgesTroughDoors)
            {
                if (e.Intersects(edge, out float t1, out float t2))
                {
                    edge.AddFeature(FeatureType.Door, t2, size);
                }
            }

            edge.FillEmptyWithWalls();

        }
    }

    public static Polygon2D CreatePolygon(Operation[] operations, bool closePoly)
    {
        if (operations == null)
            return Polygon2D.Undefined();
        if (operations.Length == 0)
            return Polygon2D.Undefined();

        //Random.InitState(Seed);
        Vector2 currentPos = Vector2.zero;

        Vector2 dir = Vector2.right;
        Polygon2D poly = new Polygon2D(EdgeType.External);
        foreach (Operation op in operations)
        {
            dir = dir.RotateRadians(op.angle * Mathf.Deg2Rad);
            currentPos += dir * op.distance;
            poly.Add(new Vertex2D(currentPos));
        }
        if (closePoly)
            poly.Close();


        return poly;
    }

    public static Polygon2D[] CreateInteriorWalls(WallOperation[] operations, Polygon2D basePolygon)
    {

        if (operations == null)
            return null;
        if (operations.Length == 0)
            return null;

        List<Polygon2D> polys = new List<Polygon2D>();


        foreach (WallOperation op in operations)
        {

            Edge2D[] baseEdges = basePolygon.GetEdges();

            int index = Mathf.Clamp(op.edgeIndex, 0, baseEdges.Length - 1);
            float edgeDist = Mathf.Clamp01(op.distance);

            Edge2D baseEdge = baseEdges[index];
            Vector2 dir = /*baseEdge.Direction*/(Vector2.right).RotateRadians(op.angle * Mathf.Deg2Rad);
            Vector2 startPos = baseEdge.Value(edgeDist);
            Vector2 endPos = startPos + dir * basePolygon.Size.magnitude; // Search value later

            Polygon2D extendedPoly = new Polygon2D(EdgeType.Internal);
            extendedPoly.Add(new Vertex2D(startPos - dir * basePolygon.Size.magnitude));
            extendedPoly.Add(new Vertex2D(endPos));
            List<Vector2> intersections = new List<Vector2>();
            foreach (Polygon2D p in polys)
            {
                intersections.AddRange(extendedPoly.IntersectionsTo(p, false, true));
            }
            intersections.AddRange(extendedPoly.IntersectionsTo(basePolygon, false, false));


            if (intersections.Count >= 2)
            {
                Vector2? closest = startPos.ClosestTo(intersections.ToArray(), true);
                if (closest != null)
                {
                    Polygon2D poly = new Polygon2D(EdgeType.Internal);
                    poly.Add(new Vertex2D(startPos));
                    poly.Add(new Vertex2D((Vector2)closest));
                    polys.Add(poly);
                }
            }
        }


        return polys.ToArray();
    }

    //public static List<Window> CreateWindows(Polygon2D facade)
    //{
    //    List<Window> windows = new List<Window>();
        



    //    return windows;
    //}

} 