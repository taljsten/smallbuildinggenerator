﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class BuildingResources
{
    public static void Init()
    {
        Meshes = new Dictionary<string, MeshExtended>();
        Materials = new List<Material>(); // new Dictionary<string, Material>();
        //Meshes.name = "floor mesh";
    }

    

    public static  Dictionary<string,MeshExtended> Meshes { get; private set; }
    public static  List<Material> Materials { get; private set; }


    public static void PopulateMeshes(BuildingData data)
    {
        Floor(data);
        InnerWalls(data);
        Ceiling(data);
        Facade(data);
        Roof(data);
        Windows(data);
        DoorFrames(data);
        DoorPanels(data);
    }

    private static void DoorPanels(BuildingData data)
    {
        int index = 0;
        foreach (MeshData door in data.DoorPanelsMeshData)
        {
            Mesh doorMesh = new Mesh();
            doorMesh.name = "DoorPanel mesh " + index;
            doorMesh.vertices = door.Vertices.ToArray();
            doorMesh.triangles = door.Triangles.ToIntArray();
            doorMesh.uv = door.UV.ToArray();
            doorMesh.RecalculateNormals();
            doorMesh.RecalculateBounds();

            //Debug.Log(doorMesh.uv.Length);
            //Debug.Log(doorMesh.vertices.Length);

            //Meshes.Add("window " + index, new MeshExtended(windowMesh, Materials[data.WindowsMeshData[index].MaterialId]));
            Meshes.Add("doorpanel " + index, new MeshExtended(doorMesh, Materials[data.DoorFramesMeshData[index].MaterialId], door.Position, door.Rotation));
            //Debug.Log(data.WindowsMeshData[index].MaterialId);
            index++;
        }
    }

    private static void DoorFrames(BuildingData data)
    {
        int index = 0;
        foreach (MeshData door in data.DoorFramesMeshData)
        {
            Mesh doorMesh = new Mesh();
            doorMesh.name = "Door mesh " + index;
            doorMesh.vertices = door.Vertices.ToArray();
            doorMesh.triangles = door.Triangles.ToIntArray();
            doorMesh.uv = door.UV.ToArray();
            doorMesh.RecalculateNormals();
            doorMesh.RecalculateBounds();

            //Debug.Log(doorMesh.uv.Length);
            //Debug.Log(doorMesh.vertices.Length);

            //Meshes.Add("window " + index, new MeshExtended(windowMesh, Materials[data.WindowsMeshData[index].MaterialId]));
            Meshes.Add("door " + index, new MeshExtended(doorMesh, Materials[data.DoorFramesMeshData[index].MaterialId], door.Position, door.Rotation));
            //Debug.Log(data.WindowsMeshData[index].MaterialId);
            index++;
        }
    }

    private static void Windows(BuildingData data)
    {
        int index = 0;
        foreach (MeshData window in data.WindowsMeshData)
        {
            Mesh windowMesh = new Mesh();
            windowMesh.name = "Window mesh " + index;
            windowMesh.vertices = window.Vertices.ToArray();
            windowMesh.triangles = window.Triangles.ToIntArray();
            windowMesh.uv = window.UV.ToArray();
            windowMesh.RecalculateNormals();
            windowMesh.RecalculateBounds();
            
            //Meshes.Add("window " + index, new MeshExtended(windowMesh, Materials[data.WindowsMeshData[index].MaterialId]));
            Meshes.Add("window " + index, new MeshExtended(windowMesh, Materials[data.WindowsMeshData[index].MaterialId], window.Position, window.Rotation));
            //Debug.Log(data.WindowsMeshData[index].MaterialId);
            index++;
        }
    }

    private static void Roof(BuildingData data)
    {
        Mesh roofTop = new Mesh();
        roofTop.name = "Floor mesh";
        roofTop.vertices = data.RoofTopData.Vertices.ToArray();
        roofTop.triangles = data.RoofTopData.Triangles.ToIntArray();
        roofTop.uv = data.RoofTopData.UV.ToArray();
        roofTop.RecalculateNormals();
        roofTop.RecalculateBounds();
        //Debug.Log(data.FloorData.MaterialId + " " + Materials.Count);
        Meshes.Add("roofTop", new MeshExtended(roofTop, Materials[data.RoofTopData.MaterialId]));

        Mesh roofSlant = new Mesh();
        roofSlant.name = "Floor mesh";
        roofSlant.vertices = data.RoofSlantData.Vertices.ToArray();
        roofSlant.triangles = data.RoofSlantData.Triangles.ToIntArray();
        roofSlant.uv = data.RoofSlantData.UV.ToArray();
        roofSlant.RecalculateNormals();
        roofSlant.RecalculateBounds();
        //Debug.Log(data.FloorData.MaterialId + " " + Materials.Count);
        Meshes.Add("roofSlant", new MeshExtended(roofSlant, Materials[data.RoofSlantData.MaterialId]));


        //data.FloorData.AssignMesh(floor);
    }

    private static void Facade(BuildingData data)
    {
        int count = 0;
        //Debug.Log(data.FacadeMeshData.Count);

        foreach (MeshData meshData in data.FacadeMeshData)
        {
            Mesh facade = new Mesh();
            facade.name = "Façade mesh " + count;
            facade.vertices = meshData.Vertices.ToArray();
            facade.triangles = meshData.Triangles.ToIntArray();
            facade.uv = meshData.UV.ToArray();
            facade.RecalculateNormals();
            facade.RecalculateBounds();
            Meshes.Add("Facade " + count, new MeshExtended(facade, Materials[meshData.MaterialId])/*facade*/);
            //meshData.Mesh = facade;
            count++;

        }
    }

    private static void InnerWalls(BuildingData data)
    {
        int count = 0;
        foreach (MeshData meshData in data.InnerWallsMeshData)
        {
            Mesh iWalls = new Mesh();
            iWalls.name = "iWalls mesh " + count;
            iWalls.vertices = meshData.Vertices.ToArray();
            iWalls.triangles = meshData.Triangles.ToIntArray();
            iWalls.uv = meshData.UV.ToArray();
            iWalls.RecalculateNormals();
            iWalls.RecalculateBounds();
            Meshes.Add("iWalls " + count, new MeshExtended(iWalls, Materials[meshData.MaterialId]));
            count++;
        }
    }

    private static void Ceiling(BuildingData data)
    {
        Mesh ceiling = new Mesh();
        ceiling.name = "Floor mesh";
        ceiling.vertices = data.CeilingData.Vertices.ToArray();
        ceiling.triangles = data.CeilingData.Triangles.ToIntArray();
        ceiling.uv = data.CeilingData.UV.ToArray();
        ceiling.RecalculateNormals();
        ceiling.RecalculateBounds();
        //Debug.Log(data.FloorData.MaterialId + " " + Materials.Count);
        Meshes.Add("ceiling", new MeshExtended(ceiling, Materials[data.FloorData.MaterialId]));
        //data.FloorData.AssignMesh(floor);
    }

    private static void Floor(BuildingData data)
    {
        Mesh floor = new Mesh();
        floor.name = "Floor mesh";
        floor.vertices = data.FloorData.Vertices.ToArray();
        floor.triangles = data.FloorData.Triangles.ToIntArray();
        floor.uv = data.FloorData.UV.ToArray();
        floor.RecalculateNormals();
        floor.RecalculateBounds();
        //Debug.Log(data.FloorData.MaterialId + " " + Materials.Count);
        Meshes.Add("floor", new MeshExtended(floor, Materials[data.FloorData.MaterialId]));
        //data.FloorData.AssignMesh(floor);
    }

    public static void AddMaterial(/*string name,*/ Material material)
    {
        Materials.Add(/*name,*/ material);
    }



}
