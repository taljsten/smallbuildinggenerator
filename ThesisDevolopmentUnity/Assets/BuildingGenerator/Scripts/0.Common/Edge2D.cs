﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum KvalueType
{
    Normal,
    X,
    Y
}

[System.Serializable]
public struct KvalueMap
{
    public KvalueMap(KvalueType kType, float value)
    {
        KType = kType;
        Value = value;
    }

    public KvalueType KType {get; set;}
    public float Value { get; set; }
}


[System.Serializable]
public struct Edge2D
{
    public Vertex2D Point2 { get; set; }
    public Vertex2D Point1 { get; set; }
    public List<Feature> features;

    public Edge2D(Vertex2D p1, Vertex2D p2, EdgeType edgeType):this()
    {
        Point1 = p1;
        Point2 = p2;
        this.edgeType = edgeType;
        features = new List<Feature>();
    }

    public void AddFeature(Feature feature)
    {        
        features.Add(feature);
    }


    public float LocalToValue(float localScalar)
    {
        return localScalar / Length;
    }

    public void AddFeature(FeatureType featureType, float pos, Vector2 size) 
    {
        AddFeature(new Feature(featureType, pos - size.x/2, pos + size.x/2, size));
    }

    public Feature[] GetFeatures()
    {
        return features.ToArray();
        
    }

    public List<Feature> GetFeatureListCopy()
    {
        List<Feature> copy = new List<Feature>();
        foreach (Feature feature in features)
        {
            copy.Add(feature);
        }

        return copy;

    }

    /// <summary>
    /// Extend symmetrical total length in world units
    /// </summary>
    /// <param name="extension"></param>
    /// <returns></returns>
    public Edge2D Extend(float extension)
    {
        float ext = extension / 2;
        Edge2D extended = new Edge2D(new Vertex2D(Point1.position - ext * Direction),
                                     new Vertex2D(Point2.position + ext * Direction),
                                     edgeType);
        return extended;
    }


    public Edge2D Offset(float distance)
    {
        Edge2D offset = new Edge2D(new Vertex2D(Point1.position + distance * OrthoDirection),
                                   new Vertex2D(Point2.position + distance * OrthoDirection),
                                   edgeType);
        return offset;
    }

    public void SetPoint2(Vertex2D vertex)
    {
        Point2 = vertex;
    }

    public void SetPoint1(Vertex2D vertex)
    {
        Point1 = vertex;
    }

    public Edge2D(float x1, float y1, float x2, float y2, EdgeType edgeType) : this(new Vertex2D(x1,y1), new Vertex2D(x2,y2), edgeType)
    {
    }

    public EdgeType edgeType;

    public Vector2 Direction
    {
        get
        {
            return DirectionNotNorm.normalized;
        }
    }

    public Vector2 OrthoDirection
    {
        get
        {
            return new Vector2(-Direction.y, Direction.x).normalized;//   Direction.RotateRadians(90 * Mathf.Deg2Rad);
            //return new Vector2(Direction.y, -Direction.x).normalized;
        }
    }

    public Vector2 DirectionNotNorm
    {
        get
        {
            return (Point2.position - Point1.position);
        }
    }

    public void FillEmptyWithWalls()
    {
        features.Sort();
        List<Feature> walls = new List<Feature>();
        float currentpos = 0;
        foreach (Feature feature in features)
        {
            walls.Add(new Feature(FeatureType.Wall, currentpos, feature.StartPos, new Vector2(feature.StartPos-currentpos, LocalToValue(BuildRules.WallHeight))));
            currentpos = feature.EndPos;
        }
        walls.Add(new Feature(FeatureType.Wall, currentpos, 1, new Vector2(1 - currentpos, LocalToValue(BuildRules.WallHeight))));

        features.AddRange(walls);
        features.Sort();
    }

    public void Translate(Vector2 amount)
    {
        Point1 = new Vertex2D(Point1.position + amount);
        Point2 = new Vertex2D(Point2.position + amount);       
    }

    public Vector2 BisectorDirection(Edge2D toedge)
    {
        return (toedge.Direction - Direction).normalized;
    }

    public float Length
    {
        get
        {
            return Vector2.Distance(Point1.position, Point2.position);
        }
    }

    public Vector2 Value(float distance)
    {
        return Point1.position + distance * DirectionNotNorm;
    }

    public float Dot(Edge2D other)
    {
        return Direction.x * other.Direction.x + Direction.y * other.Direction.y;
    }

    public bool Intersects(Vertex2D vertex)
    {
        return (Split(vertex) != null);
    }

    public float AngleTo(Edge2D other)
    {
        return Direction.RealAngleTo(other.Direction);
    }

    public bool Intersects(Vertex2D vertex, out float t)
    {
        Vector2 orthoDir = Direction.RotateRadians(90 * Mathf.Rad2Deg);
        Edge2D edge = new Edge2D(new Vertex2D(vertex.position - orthoDir * 0.2f), new Vertex2D(vertex.position + orthoDir * 0.2f), edgeType);
        return Intersects(edge, out t, out float _);
    }

    public float Kvalue
    {
        get
        {
            float deltaY = Point2.position.y - Point1.position.y;
            float deltaX = Point2.position.x - Point1.position.x;
            if (Mathf.Approximately(deltaX, 0f))
                return float.PositiveInfinity;
            else
                return deltaY / deltaX;
        }
    }

    public float Mvalue
    {
        get
        {
            return Point1.position.y - Kvalue * Point1.position.x;
        }
    }

    public bool GetIntersection(Edge2D other, out Vector2 intersection)
    {
        intersection = Vector2.zero;
        if (Mathf.Approximately(Kvalue, other.Kvalue))
            return false;
        float x = (Mvalue - other.Mvalue) / (other.Kvalue - Kvalue);
        float y = Kvalue * x + Mvalue;
        intersection = new Vector2(x, y);
        return true;

    }

    //public bool GetIntersection(Edge2D other, out Vector2 intersection)
    //{
    //    intersection = Vector2.zero;

    //    float tX = (other.Point1.position.x - other.Point1.position.x) / 

    //}

    public bool Intersects(Edge2D other, out float t1, out float t2)
    {
        
        t2 = -1f; t1 = -1f;
        // parallell
        if (Mathf.Approximately(this.Dot(other), 1f))
        {
            //Debug.Log("Intersection Edges are paralell: " + this + " vs " + other);      
            
            return false;
        }

        Vector2 p1 = Point1.position;
        Vector2 d1 = DirectionNotNorm;
        Vector2 p2 = other.Point1.position;
        Vector2 d2 = other.DirectionNotNorm;

        float t_num = d1.x * (p2.y - p1.y) + d1.y * (p1.x - p2.x);
        float t_den = d1.y * d2.x - d1.x * d2.y;
        if (Mathf.Approximately(t_den, 0f) )
        {
            //Debug.Log("Intersection Denominator is 0: " + this + " vs " + other);
            return false;
        }
            
        t2 = t_num / t_den;

        

        if (Mathf.Approximately(d1.y, 0f))
            t1 = (p2.x - p1.x + t2 * d2.x) / d1.x;
        else
            t1 = (p2.y - p1.y + t2 * d2.y) / d1.y;

        if (t1 >= 0f && t1 <= 1f && t2 >= 0f && t2 <= 1)
            return true;
        else
            return false;
    }

    

    public Edge2D[] Split(Vertex2D vertex)
    {

        if (Vector2.Distance(vertex.position, Point1.position) < 0.01f || 
            Vector2.Distance(vertex.position, Point2.position) < 0.01f)
            return null;
        
        
        Vector2 tVector = new Vector2();
        float t = -1;
        //bool couldSplit = false;



        if (Mathf.Abs(DirectionNotNorm.x) < 0.01f)
        {
            //Debug.Log("First if");
            tVector.y = (vertex.position.y - Point1.position.y) / DirectionNotNorm.y;
            tVector.x = tVector.y;
        }
        else if(Mathf.Abs(DirectionNotNorm.y) < 0.01f)
        {
            //Debug.Log("Second if");
            tVector.x = (vertex.position.x - Point1.position.x) / DirectionNotNorm.x;
            tVector.y = tVector.x;
        }
        else
        {
            //Debug.Log("Third if - " + vertex.position + " " + point1 + " " + DirectionNotNorm);
            tVector.x = (vertex.position.x - Point1.position.x) / DirectionNotNorm.x;
            tVector.y = (vertex.position.y - Point1.position.y) / DirectionNotNorm.y;
        }

        //Debug.Log(tVector);

        if (Mathf.Abs(tVector.x - tVector.y) < 0.01f && tVector.x >= 0 && tVector.x <= 1)
        {
            
            t = tVector.x;
            return Split(t);
        }
        else
            return null;

        
    }

    public Edge2D[] Split(float distance)
    {
        float d = Mathf.Clamp(distance, 0.01f, 0.99f);

        Vector2 splitPoint = Value(d);
        Edge2D[] newEdges = new Edge2D[2];
        newEdges[0] = new Edge2D(Point1, new Vertex2D(splitPoint), edgeType);
        newEdges[1] = new Edge2D(new Vertex2D(splitPoint), Point2, edgeType);

        return newEdges;
    }
    

    public override string ToString()
    {
        return "Edge from " + Point1 + " to " + Point2;
    }
}


