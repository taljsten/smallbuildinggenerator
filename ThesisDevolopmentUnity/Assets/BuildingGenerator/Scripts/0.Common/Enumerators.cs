﻿
public enum EdgeType
{
    Internal,
    External
}

public enum FeatureType
{
    Door,
    Window,
    Wall
}

[System.Serializable]
public enum OffsetMethod
{
    Bisector,
    Edge,
    Ortho
}

