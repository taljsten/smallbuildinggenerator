﻿
using System;
using System.Collections.Generic;
using UnityEngine;

public struct Feature : IComparable
{
    public Feature(FeatureType type, float startpos, float endpos, Vector2 size)
    {
        Type = type;
        StartPos = startpos;
        EndPos = endpos;
        Position = (startpos + endpos)/2;


        Size = size;
    }

    public FeatureType Type { get; set; }
    public float Position { get; set; }
    public float StartPos { get; set; }
    public float EndPos { get; set; }
    public Vector2 Size { get; set; }
   

    public int CompareTo(object obj)
    {
        if (obj == null)
            return 1;
        Feature other = (Feature)obj;

        return Position.CompareTo(other.Position);

    }

    public override string ToString()
    {
        return Type.ToString() + " Pos : " + Position + " (" + StartPos + "; " + EndPos + ")"; ;
    }
}


