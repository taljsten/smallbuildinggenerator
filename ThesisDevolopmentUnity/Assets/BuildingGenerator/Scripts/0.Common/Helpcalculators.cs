﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class Helpcalculators : MonoBehaviour
{
    public Vector2 vector= new Vector2();
    public Vector2 normalized;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        normalized = vector.normalized;
    }
}
