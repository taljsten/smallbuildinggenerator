﻿using UnityEngine;
using UnityEditor;

public struct MeshData
{


    public MeshData(Vector3[] vertices, Vector2[] uV, Triangle[] triangles)
    {
        Vertices = vertices;
        UV = uV;
        Triangles = triangles;
        MaterialId = -1;
        Mesh = new Mesh();
        Position = Vector3.zero;
        Rotation = Quaternion.identity;
    }

    public MeshData(Vector3[] vertices, Vector2[] uV, Triangle[] triangles, int materialId, Vector3 position, Quaternion rotation) : this(vertices, uV, triangles)
    {
        MaterialId = materialId;
        Mesh = new Mesh();
        Position = position;
        Rotation = rotation;
    }

    public Vector3[] Vertices { get; set; }
    public Vector2[] UV { get; set; }
    public Triangle[] Triangles { get; set; }
    public int MaterialId { get; set; }
    public Mesh Mesh { get; set; }
    public Vector3 Position { get; set; }
    public Quaternion Rotation { get; set; }

    public void AssignMesh(Mesh mesh)
    {
        Mesh = mesh;
    }

    public void DrawGizmos()
    {
        if (Vertices!=null)
        {
            int index = 0;
            Handles.color = Color.red;
            foreach (Vector3 vert in Vertices)
            {
               // Gizmos.DrawWireSphere(vert, 0.1f);
                Handles.Label(vert + Position/*+ Vector3.right*.3f*/, (index++).ToString());
            }

            if (Triangles != null)
            {
                //Debug.Log(Triangles.Length);
                int i = 0;
                foreach (Triangle triangle in Triangles)
                {
                    if (true)
                    {
                        Vector3 a0 = Vertices[triangle.v0] + Position;
                        Vector3 a1 = Vertices[triangle.v1] + Position;
                        Vector3 a2 = Vertices[triangle.v2] + Position;

                        Gizmos.DrawLine(a0, a1); 
                        Gizmos.DrawLine(a1, a2);
                        Gizmos.DrawLine(a2, a0);

                        //Gizmos.DrawWireSphere(a0, 0.1f);
                        //Gizmos.DrawWireSphere(a1, 0.1f);
                        //Gizmos.DrawWireSphere(a2, 0.1f); 
                    }
                    i++;
                }
            }
        }

        
    }
}
