﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


public class Polygon2D : IDrawGizmos
{

    private List<Vertex2D> vertices;
    private List<Edge2D> edges;

    
    private EdgeType edgeType;
    public List<Vector2> Intersections { get; set; }
    public int NumberOverlappedVertices { get; private set; }

    public Polygon2D Copy(EdgeType edgeType)
    {
        Polygon2D copy = new Polygon2D(edgeType);
        foreach (Vertex2D vertex in GetVertices())
        {
            copy.Add(vertex);
        }
        if (IsClosed())
            copy.Close();
        return copy;
    }

    

    public bool IsUndefined { get; private set; }

    public static Polygon2D Undefined()
    {
        Polygon2D defPoly = new Polygon2D(default);

        defPoly.Add(new Vertex2D(0, 0));
        defPoly.Add(new Vertex2D(1, 0));
        defPoly.Add(new Vertex2D(1, 1));
        defPoly.Close();
        defPoly.IsUndefined = true;

        return defPoly;
       
    }

    public void Clear()
    {
        vertices.Clear();
        edges.Clear();
    }

    public int NumberSmallAngles { get; private set; }

    public Polygon2D(EdgeType edgeType, Vector2[] points, bool close):this(edgeType)
    {
        if (points != null)
        {
            foreach (Vector2 p in points)
            {
                Add(p);
            }
            if (close)
                Close(); 
        }
    }

    public Polygon2D(EdgeType edgeType)
    {
        vertices = new List<Vertex2D>();
        edges = new List<Edge2D>();
        Intersections = new List<Vector2>();
        IsUndefined = false;
        this.edgeType = edgeType;
    }

    public void Add(float x, float y)
    {
        Add(new Vertex2D(x,y));
    }

    public void Add(Vector2[] poses)
    {
        foreach (Vector2 pos in poses)
        {
            Add(pos);
        }
    }

    public void Add(Vector2 pos)
    {
        Add(new Vertex2D(pos));
    }

    public void Add(Vertex2D vertex)
    {
        vertices.Add(vertex);
        if (vertices.Count > 1)
        {
            edges.Add(new Edge2D(vertices[vertices.Count - 2], vertices[vertices.Count - 1], edgeType));
            
        }
    }

    public void Insert(Vertex2D vertex)
    {
        int index = -1;
        Edge2D edge;
        for (int i = 0; i < edges.Count; i++)
        {
            if (edges[i].Intersects(vertex))
            {
                index = i;
                edge = edges[i];
                break;
            }
        }

        if (index != -1)
        {
            vertices.Insert(index+1, vertex);
            Vertex2D old2 = edges[index].Point2;
            edges[index].SetPoint2(vertex);
            Edge2D e = new Edge2D(vertex, old2, edgeType);
            
            edges.Insert(index + 1, e);
        }

    }

    public Polygon2D ExtendAllEdges(float v)
    {
        throw new NotImplementedException();
    }

    public bool IsClosed()
    {
        if (edges==null || edges.Count < 3)
            return false;
        
        if (edges[0].Point1.Overlaps(edges[edges.Count - 1].Point2))
            return true;
        else
            return false;
    }


    public Polygon2D Offset(float distance, OffsetMethod method)
    {
        Polygon2D offsetPoly = new Polygon2D(edgeType);

        if (edges.Count > 1)
        {
            if (IsClosed())
            {
                if (method == OffsetMethod.Ortho)
                {
                    
                    //Debug.Log("Generating offset start");
                    distance = -distance;
                    for (int i = 0; i < edges.Count; i++)
                    {
                        int j = i - 1;
                        if (j == -1)
                            j = edges.Count - 1;



                        Vector2 commonPoint = edges[i].Point1.position;
                        Vector2 pointJ = commonPoint + edges[j].OrthoDirection * distance;
                        Edge2D edgeJ = new Edge2D(new Vertex2D(pointJ - edges[j].Direction * 5000f),
                                                  new Vertex2D(pointJ + edges[j].Direction * 5000f),
                                                  edgeType);
                        Vector2 pointI = commonPoint + edges[i].OrthoDirection * distance;
                        Edge2D edgeI = new Edge2D(new Vertex2D(pointI - edges[i].Direction * 5000f),
                                                  new Vertex2D(pointI + edges[i].Direction * 5000f),
                                                  edgeType);

                        //if (edgeI.GetIntersection(edgeJ, out Vector2 intersection))
                        //{
                        //    offsetPoly.Add(new Vertex2D(intersection));
                        //}

                        if (edgeJ.Intersects(edgeI, out float t1, out float t2))
                        {
                            //Debug.Log(edgeJ.Value(t1) + " : " + commonPoint + " : " + edgeI.Value(t2));
                            offsetPoly.Add(new Vertex2D(edgeI.Value(t2)));
                        }


                    }
                    offsetPoly.Close();
                    //Debug.Log("Generating offset done");
                }
                else 
                { 
                    for (int i = 0; i < edges.Count; i++)
                    {
                        int j = i - 1;
                        if (j == -1)
                            j = edges.Count - 1;


                        Vector2 localDist;
                        Vector2 dir = edges[i].BisectorDirection(edges[j]);
                        switch (method)
                        {
                            default:
                            case OffsetMethod.Bisector:

                                if (!edges[i].Point1.IsConvex)
                                    dir = -dir;
                                localDist = distance * dir;
                                break;
                            case OffsetMethod.Edge:
                                float halfAngle = edges[i].AngleTo(edges[j]) / 2f;
                                //localDist = (edges[i].Direction - edges[j].Direction).normalized * distance / Mathf.Sin(halfAngle) ;

                                localDist = -dir * distance / Mathf.Sin(halfAngle);
                                //if (!edges[i].Point1.IsConvex)
                                //    localDist = -localDist;
                                break;

                        }

                        offsetPoly.Add(new Vertex2D(edges[i].Point1.position + localDist));
                    }
                    offsetPoly.Close();  
                }
            }
            else
            {
                throw new NotImplementedException("unclosed polys cannot be offseted yet");
            }
        }

        

        return offsetPoly;
    }

    public Vertex2D VertexAt(int i)
    {
        if (vertices != null)
            return vertices[i];
        else
            return null;
    }

    public bool HasVertex(Vertex2D vertex)
    {
        foreach (Vertex2D v in vertices)
        {
            if (Vector2.Distance(v.position, vertex.position) < 0.01f)
                return true;
        }
        return false;
    }

    public bool Close()
    {
        if (vertices.Count <= 1)
            return false;

        edges.Add(new Edge2D(vertices[vertices.Count - 1], vertices[0], edgeType));

        CalculateVerticesAngles();
        CalculateSelfIntersections();
        CountOverlaps();
        CountZeroAngles();

        return true;
    }

    private void CountZeroAngles()
    {
        NumberSmallAngles = 0;
        foreach (Vertex2D vertex in vertices)
        {
            if (Mathf.Abs(vertex.angle * Mathf.Rad2Deg) < 5f)
                NumberSmallAngles++;
        }
    }

    private void CountOverlaps()
    {
        NumberOverlappedVertices = 0;
        for (int i = 0; i < vertices.Count-1; i++)
        {
            for (int j = i+1; j < vertices.Count; j++)
            {
                if (vertices[i].Overlaps(vertices[j]))
                    NumberOverlappedVertices++;
            }
        }
    }

    private void CalculateVerticesAngles()
    {
        for (int i = 0; i < edges.Count; i++)
        {
            int j = i - 1;
            if (i == 0)
                j = edges.Count - 1;

            //vertices[i].SetAngle(Vector2.Angle(-edges[j].Direction, edges[i].Direction));
            vertices[i].SetAngle(edges[j].Direction.RealAngleTo(edges[i].Direction));
            
        }
    }

    private void CalculateSelfIntersections()
    {
        if (edges.Count > 4)
        {
            for (int i = 0; i < edges.Count - 1; i++)
            {
                for (int j = i + 2; j < edges.Count; j++)
                {
                    if ( !(i == 0 && j == edges.Count - 1))
                    {
                        if (edges[i].Intersects(edges[j], out float t1, out float t2))
                        {
                            Intersections.Add(edges[j].Value(t2));
                        } 
                    }
                }
            } 
        }
    }    

    public bool IsPointInsidePolygon(Vector2 point)
    {
        if (!IsClosed())
            return false;
        Polygon2D poly = new Polygon2D(EdgeType.External);
        poly.Add(new Vertex2D(point));
        poly.Add(new Vertex2D(point + Size.x * 10f * Vector2.right));
        int numberOfIntersections = IntersectionsTo(poly, false, false).Length;
        if ((numberOfIntersections & 1) == 0)
            return false;
        else
            return true;
        
    }

    public Vector2[] IntersectionsTo(Polygon2D other, bool addIntersectionsTothis, bool addIntersectionsToOther)
    {
        List<Vector2> intersects = new List<Vector2>();
        for (int i = 0; i < edges.Count; i++)
        {
            for (int j = 0; j < other.edges.Count; j++)
            {
                if (edges[i].Intersects(other.edges[j], out float t1, out float t2))
                {
                    intersects.Add(other.edges[j].Value(t2));
                }
            }
        }
        if (addIntersectionsToOther)
        {
            other.Intersections.AddRange(intersects);
        }
        if(addIntersectionsTothis)
        {
            Intersections.AddRange(intersects);
        }
        return intersects.ToArray();
    }

    public int NumCorners
    {
        get
        {
            return vertices.Count;
        }
    }

    public int NumConcaveCorners
    {
        get
        {
            int num = 0;
            foreach (Vertex2D vertex in vertices)
            {
                if (!vertex.IsConvex)
                    num++;
            }
            return num;
        }
    }    

    public Vector2[] VerticesToVectors()
    {
        List<Vector2> verts = new List<Vector2>();
        foreach(Vertex2D vertex in vertices)
        {
            verts.Add(vertex.position);
        }
        return verts.ToArray();
    }

    public Edge2D[] GetEdges()
    {
        return edges.ToArray();
    }


    public float ShortestEdgeLength()
    {
        float shortest = float.MaxValue;
        foreach (Edge2D edge in edges)
        {
            if (edge.Length < shortest)
                shortest = edge.Length;
        }
        return shortest;
    }

    public Vertex2D[] GetVertices()
    {
        return vertices.ToArray();
    }

    public Vector2 Max
    {
        get
        {
            float x = float.MinValue;
            float y = float.MinValue;
            foreach (Vector2 vector in VerticesToVectors())
            {
                if (vector.x > x)
                    x = vector.x;
                if (vector.y > y)
                    y = vector.y;
            }

            return new Vector2(x, y);

        }
    }

    public Vector2 Min
    {
        get
        {
            float x = float.MaxValue;
            float y = float.MaxValue;
            foreach (Vector2 vector in VerticesToVectors())
            {
                if (vector.x < x)
                    x = vector.x;
                if (vector.y < y)
                    y = vector.y;
            }

            return new Vector2(x, y);

        }
    }

    public Vector2 Size
    {
        get
        {
            return (Max - Min);
        }
    }

    
    public Vector2[] GetUVs(Rect fitTo, float scale)
    {
        float xScale = fitTo.width / Size.x;
        float yScale = fitTo.height / Size.y;
        float scaleTot = Mathf.Min(xScale, yScale) * scale;
        Vector2 offset = fitTo.position - Min;

        return GetUVs(scaleTot, offset);

    }

    public Vector2[] GetUVs(float scale, Vector2 offset)
    {
        Vector2[] uv = VerticesToVectors();
        for (int i = 0; i < uv.Length; i++)
        {
            uv[i] = uv[i] * scale + offset;
        }

        return uv;
    }

    public Vector2[] GetUVs(float scale)
    {
        return GetUVs(scale, Vector2.zero);
    }

    public Vector2[] GetUVs()
    {
        return GetUVs(1.0f);
    }


    public Polygon2D Rotate(float radians)
    {
        Polygon2D rotated = new Polygon2D(edgeType);
        Vector2[] points = GetVectors();
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = points[i].RotateRadians(radians);
            rotated.Add(points[i]);
        }
        if (IsClosed())
            rotated.Close();

        return rotated;
    }

    private Vector2[] GetVectors()
    {
        Vector2[] vectors = new Vector2[vertices.Count];
        for (int i = 0; i < vectors.Length; i++)
        {
            vectors[i] = vertices[i].position;
        }
        return vectors;
    }

    public MeshData Extrude(float dist, bool centered)
    {
        return Extrude(dist, centered, 1, 1, 0, 0);
    }

    public MeshData Extrude(float dist, bool centered, float materialscale, float trimheight, float trimpos, float uvRot)
    {
        float dist1 = 0;
        float dist2 = dist;
        if (centered)
        {
            dist1 = -dist / 2;
            dist2 = dist / 2;
        }
        Vector3[] verts = new Vector3[vertices.Count * 6]; // 3 verts in every corner (sharp edges)
        int index = 0;
        
        // Front
        foreach (Vertex2D vertex in vertices)
        {
            verts[index++] = new Vector3(vertex.position.x, vertex.position.y, dist1);

        }

        // Back
        foreach (Vertex2D vertex in vertices)
        {
            verts[index++] = new Vector3(vertex.position.x, vertex.position.y, dist2);

        }

        PolygonSubdivision subdiv = new PolygonSubdivision(this, false);
        PolygonSubdivision subdivR = new PolygonSubdivision(this, true);
        List<Triangle> triangles = new List<Triangle>();
        triangles.AddRange(subdiv.Triangles);                           // Front
        triangles.AddRange(subdivR.GetTriangles(vertices.Count));    // Back

        // Strips (lika många strips som verts i polygonen)
        //List<float> lengths = new List<Vector2>(); // used for uv layout later
        for (int i = 0; i < vertices.Count; i++)
        {
            int j = i + 1;
            if (j == vertices.Count)
                j = 0;

            int i0 = index;
            int i1 = index + 1;
            int i2 = index + 2;
            int i3 = index + 3;

            verts[i0] = new Vector3(vertices[i].position.x, vertices[i].position.y, dist1);
            verts[i1] = new Vector3(vertices[j].position.x, vertices[j].position.y, dist1);
            verts[i2] = new Vector3(vertices[i].position.x, vertices[i].position.y, dist2);
            verts[i3] = new Vector3(vertices[j].position.x, vertices[j].position.y, dist2);

            triangles.Add(new Triangle(i0, i1, i3));
            triangles.Add(new Triangle(i0, i3, i2));
            index += 4;

            //lengths.Add(Vector3.Distance(verts[i0], verts[i1]));
        }
        //index -= 4;

        List<Vector2> uv = new List<Vector2>(); // new Vector2[verts.Length];

        Polygon2D rotated = Rotate(uvRot);

        float scale = materialscale * (trimheight / rotated.Size.y);
        Vector2 min = rotated.Min * scale;
        Vector2 offset = new Vector2(0, trimpos - min.y);

        uv.AddRange(rotated.GetUVs(scale, offset));
        uv.AddRange(rotated.GetUVs(scale, offset));
        int start = uv.Count;

       

        for (int i = start; i < index; i+=4)
        {
            float length = Vector3.Distance(verts[i], verts[i + 1]);
            uv.Add(new Vector2(0, 0) * scale + offset);
            uv.Add(new Vector2(length, 0) * scale + offset );
            uv.Add(new Vector2(0, dist) * scale + offset);
            uv.Add(new Vector2(length, dist) * scale + offset);
        }


        MeshData meshData = new MeshData(verts, uv.ToArray(), triangles.ToArray());
        return meshData;
    }

    public void DrawGizmos(object data)
    {
        //Vertex2D[] verts = vertices.ToArray();
        //Edge2D[] edges = GetEdges();
       
        float yPos = (float)data;

        Gizmos.color = Color.blue;

        foreach (Edge2D edge in edges)
        {
            Vector3 p1 = new Vector3(edge.Point1.position.x, yPos, edge.Point1.position.y);
            Vector3 p2 = new Vector3(edge.Point2.position.x, yPos, edge.Point2.position.y);
            Gizmos.DrawLine(p1, p2);
            //  Debug.Log(edge);
        }

        foreach (Vertex2D vertex in vertices)
        {
            if (vertex.IsConvex)
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;
            Vector3 p1 = new Vector3(vertex.position.x, yPos, vertex.position.y);
            Gizmos.DrawSphere(p1, .05f);
            Handles.Label(p1 + Vector3.up * 0.2f, (vertex.angle * Mathf.Rad2Deg).ToString("0.00"));
        }

        Gizmos.color = Color.yellow;
        foreach (Vector2 v in Intersections)
        {
            Vector3 p = new Vector3(v.x, yPos, v.y);
            Gizmos.DrawWireSphere(p, .5f);
        }




    }

}
