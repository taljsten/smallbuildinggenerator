﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonSubdivision : IDrawGizmos

{
    private Polygon2D polygon;
    public List<Triangle> Triangles { get; private set; }
    public float GizmoY { get; set; } = 0.5f;
    
    public PolygonSubdivision(Polygon2D polygon):this(polygon, false)
    {
       
    }

    public PolygonSubdivision(Polygon2D polygon, bool reverse)
    {
        this.polygon = polygon;
        Triangles = new List<Triangle>();
        SubDivide(reverse);
    }

    public List<Triangle> GetTriangles(int addIndex)
    {
        List<Triangle> tris = new List<Triangle>();
        foreach (Triangle triangle in Triangles)
        {
            tris.Add(new Triangle
                (
                    triangle.v0 + addIndex,
                    triangle.v1 + addIndex,
                    triangle.v2 + addIndex
                )); 
        }
        return tris;
       
    }

    private void SubDivide(bool reverse)
    {
        if (polygon == null)
            return;

        Vertex2D[] vertices = polygon.GetVertices();
        


        if (vertices == null)
            return;

        if (vertices.Length < 3)
            return;
        
        if (vertices.Length == 3)
        {
            // special case triangle
            return;
        }
               
        int index = 0;
        if (polygon.NumConcaveCorners > 0)
        {           
            for (int i = 0; i < vertices.Length; i++)
            {
                Vertex2D vertex = vertices[i];
                if (!vertex.IsConvex)
                {
                    index = i;
                    break;
                }               
            }
        }

        TriangulateFromVertex(index, vertices, reverse);
    }

    private void TriangulateFromVertex(int index, Vertex2D[] vertices, bool reverse)
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            int i0 = index;
            int i1 = i;
            int i2 = i + 1;

            if (i2 == vertices.Length)
                i2 = 0;

            if (i0 != i1 && i0!= i2)
            {
                if (reverse)
                    Triangles.Add(new Triangle(i0, i1, i2));
                else
                    Triangles.Add(new Triangle(i0, i2, i1));
            }
            
        }
    }


    public void DrawGizmos(object data)
    {
        Vertex2D[] vertices = (Vertex2D[])data;
        Gizmos.color = Color.white;
        foreach (Triangle triangle in Triangles)
        {
            Vector3 a0 = new Vector3(vertices[triangle.v0].position.x, GizmoY, vertices[triangle.v0].position.y);
            Vector3 a1 = new Vector3(vertices[triangle.v1].position.x, GizmoY, vertices[triangle.v1].position.y);
            Vector3 a2 = new Vector3(vertices[triangle.v2].position.x, GizmoY, vertices[triangle.v2].position.y);

            Gizmos.DrawLine(a0, a1);
            Gizmos.DrawLine(a1, a2);
            Gizmos.DrawLine(a2, a0);
        }
    }
}
