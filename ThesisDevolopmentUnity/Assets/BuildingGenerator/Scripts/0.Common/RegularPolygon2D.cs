﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularPolygon2D : Polygon2D
{
    //private Vector2 pos;
    private float edgeLength; // same as outer radius
    private int numEdges;
    public MeshData MeshData { get; private set; }


    public RegularPolygon2D(EdgeType edgeType, float edgeLength, int numEdges) : base(edgeType)
    {
        //this.pos = pos;
        this.edgeLength = edgeLength;
        this.numEdges = numEdges;
        Construct();
        //Meshify();
    }   

    private void Construct()
    {

        if (numEdges < 3)
            numEdges = 3;
        if (edgeLength <= 0)
            edgeLength = .01f;

        float angle = Mathf.PI * 2 / numEdges;
        //List<Operation> operations = new List<Operation>();

        for (int i = 0; i < numEdges; i++)
        {
            Vector2 point = /*pos + */(Vector2.right * edgeLength).RotateRadians(i * angle);
            Add(new Vertex2D(point));
        }
        Close();
        
    }

    public void Meshify(Vector3 origo, Vector3 upDir, Vector3 rightDir)
    {
        Vector3[] verts = new Vector3[numEdges + 1];
        verts[0] = origo;
        Vertex2D[] polyVerts = GetVertices();
        for (int i = 0; i < numEdges; i++)
        {
            //verts[i + 1] = verts[0] + polyVerts[i].position.y * upDir + polyVerts[i].position.x * rightDir;
            verts[i + 1] = verts[0] + new Vector3(polyVerts[i].position.x, 0, polyVerts[i].position.y);
        }

        Debug.Log("Triangles:");
        List<Triangle> triangles = new List<Triangle>();
        for (int i = 1; i < verts.Length; i++)
        {
            int i1 = 0;
            int i2 = i;
            int i3 = i + 1;
            if (i3 == verts.Length)
                i3 = 1;
            Triangle t = new Triangle(i1, i2, i3);
            triangles.Add(t);
            Debug.Log(t);
        }
        Debug.Log("  ------   ");

        MeshData = new MeshData(verts, null, triangles.ToArray());
    }


}
