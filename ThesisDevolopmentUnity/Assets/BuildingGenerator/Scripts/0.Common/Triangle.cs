﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Triangle
{
    public int v0;
    public int v1;
    public int v2;

    public Triangle(int v0, int v1, int v2)
    {
        this.v0 = v0;
        this.v1 = v1;
        this.v2 = v2;
    }

    public override string ToString()
    {
        return "Triangle : " + v0 + " - " + v1 + " - " + v2;
    }
}
