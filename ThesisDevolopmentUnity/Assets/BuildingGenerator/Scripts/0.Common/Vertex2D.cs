﻿using System;
using UnityEngine;

public class Vertex2D
{
    public Vector2 position;
    public float angle;

    public Vertex2D(float x, float y) : this(new Vector2(x,y))
    {
    }

    public Vertex2D(Vector2 position) 
    {
        this.position = position;
        angle=0;
    }

    public bool IsConvex
    {
        get
        {
            return angle > 0;
        }
    }

  

    public override string ToString()
    {
        return position.ToString();
    }

    public void SetAngle(float angle)
    {
        //Debug.Log("Trying to set angle " + angle);
        this.angle = angle;
    }

    public bool Overlaps(Vertex2D v2)
    {
        Vector2 v = position - v2.position;

        return (Mathf.Approximately(v.x, 0f) && Mathf.Approximately(v.y, 0f));
    }

   
}


