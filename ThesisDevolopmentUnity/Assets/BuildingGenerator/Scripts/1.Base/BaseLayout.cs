﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public class BaseLayout : IDrawGizmos
{
    public BaseLayout(Operation[] operations)
    {
        Operations = operations;
        Polygon = BuildingFactory.CreatePolygon(Operations, true);
        Subdivision = new PolygonSubdivision(Polygon);
        Angles = new float[operations.Length];
        int i = 0;
        float lastAngle=0;
        foreach (Operation operation in operations)
        {
            Angles[i] = operation.angle+lastAngle;
            lastAngle = Angles[i++];
        }

    }

    public Operation[] Operations { get; private set; }
    public Polygon2D Polygon { get; private set; }
    public PolygonSubdivision Subdivision { get; private set; }
    public float[] Angles { get; private set; }

    public void DrawGizmos(object data)
    {
        bool show = (bool)data;

        if (show)
        {
            if (Polygon != null)
                Polygon.DrawGizmos(0f);
            if (Subdivision != null)
                Subdivision.DrawGizmos(Polygon.GetVertices()); 
        }

    }
}