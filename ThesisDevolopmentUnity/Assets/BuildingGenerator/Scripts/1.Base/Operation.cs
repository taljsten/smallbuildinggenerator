﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[System.Serializable]
public class Operation
{
    

    public float distance;
    public float angle;

    public Operation() : this(0,0)
    {

    }
    
    public Operation(float distance, float angle)
    {
        this.distance = distance;
        this.angle = angle;
    }
}
