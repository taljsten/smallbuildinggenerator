﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaterialSettings
{
    [SerializeField] private Material floorMaterial;
    [SerializeField] private Material interiorTrimMaterial;
    [SerializeField] private Material facadeTrimMaterial;
    [SerializeField] private Material roofMaterial;
    [SerializeField] private float innerWallsTrimPos  = 0;
    [SerializeField] private float innerWallsTrimHeight  = 0.25f;
    [SerializeField] private float innerWallsMaterialScale  = 1;
    [SerializeField] private float outerWallsTrimPos  = 0;
    [SerializeField] private float outerWallsTrimHeight = 0.25f;
    [SerializeField] private float outerWallsMaterialScale  = 1;
    [SerializeField] private float floorMaterialScale  = 1;
    [SerializeField] private float roofMaterialScale  = 1;


    public float InnerWallsTrimPos { get => innerWallsTrimPos; set => innerWallsTrimPos = value; }
    public float InnerWallsTrimHeight { get => innerWallsTrimHeight; set => innerWallsTrimHeight = value; }
    public float InnerWallsMaterialScale { get => innerWallsMaterialScale; set => innerWallsMaterialScale = value; }
    public float OuterWallsTrimPos { get => outerWallsTrimPos; set => outerWallsTrimPos = value; }
    public float OuterWallsTrimHeight { get => outerWallsTrimHeight; set => outerWallsTrimHeight = value; }
    public float OuterWallsMaterialScale { get => outerWallsMaterialScale; set => outerWallsMaterialScale = value; }
    public float FloorMaterialScale { get => floorMaterialScale; set => floorMaterialScale = value; }
    public float RoofMaterialScale { get => roofMaterialScale; set => roofMaterialScale = value; }
}
