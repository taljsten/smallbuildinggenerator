﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InteriorLayout : IDrawGizmos
{
    public Polygon2D[] Walls { get; private set; }

    public InteriorLayout(WallOperation[] interiorOperations, Polygon2D basePolygon, float mindoordistance)
    {
        Walls = BuildingFactory.CreateInteriorWalls(interiorOperations, basePolygon);
        BuildingFactory.CreateDoorFeatures(Walls, mindoordistance); // Doors
    }

    public int NumWalls 
    {
        get
        {
            if (Walls != null)
                return Walls.Length;
            else
                return 0;
        }
    }

    private Vector2[] GetFeaturePositions(FeatureType featureType)
    {
        List<Vector2> positions = new List<Vector2>();
        if (Walls != null)
        {
            foreach (Polygon2D wall in Walls)
            {
                foreach (Edge2D edge in wall.GetEdges())
                {
                    foreach (Feature feature in edge.GetFeatures())
                    {
                        if (feature.Type == featureType)
                            positions.Add(edge.Value(feature.Position));
                    }

                }
            } 
        }

        return positions.ToArray();
    }

    public int NumberOfFeatures
    {
        get
        {
            int count = 0;
            foreach (Polygon2D wall in Walls)
            {
                foreach (Edge2D edge in wall.GetEdges())
                {
                    count += edge.GetFeatures().Length;
                }
            }
            return count;
        }
    }

    public int NumEdges 
    {
        get 
        {
            if (Walls != null)
            {
                int count = 0;

                foreach (Polygon2D polygon in Walls)
                {
                    count += polygon.GetEdges().Length;
                }
                return count;
            }
            else
                return 0;

        }
            
    }

    public List<Vector2> Intersections 
    { 
        get
        {
            List<Vector2> intersections = new List<Vector2>();
            foreach (Polygon2D w in Walls)
            {
                intersections.AddRange(w.Intersections);
            }

            return intersections;
        }
        
    }

    public void DrawGizmos(object data)
    {
        bool show = (bool)data;
        if (Walls != null && show)
        {
            foreach (Polygon2D wall in Walls)
            {
                if (wall != null)
                    wall.DrawGizmos(1f);
            } 
        }

        if (show)
        {
            foreach (Vector2 posv2 in GetFeaturePositions(FeatureType.Door))
            {
                Gizmos.color = Color.magenta;
                Vector3 pos = new Vector3(posv2.x, 1, posv2.y);
                Gizmos.DrawWireCube(pos, new Vector3(.8f, .4f, .8f));
            } 
        }
    }

    
}