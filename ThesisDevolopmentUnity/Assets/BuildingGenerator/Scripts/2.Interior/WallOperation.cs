﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WallOperation : Operation
{
    public int edgeIndex;
    //public float distance;    
    //public float angle;
}
