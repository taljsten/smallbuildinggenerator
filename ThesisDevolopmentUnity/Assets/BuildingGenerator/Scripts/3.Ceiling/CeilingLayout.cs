﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CeilingLayout : IDrawGizmos
{

    public Polygon2D Polygon { get; private set; }
    public PolygonSubdivision Subdivision { get; private set; }

    public CeilingLayout(BaseLayout baseLayout)
    {
        Polygon = baseLayout.Polygon.Copy(EdgeType.Internal);
        Subdivision = new PolygonSubdivision(Polygon, true);
    }

    public void DrawGizmos(object data)
    {
        bool show = (bool)data;

        if (show)
        {
            if (Polygon != null)
                Polygon.DrawGizmos(4f);
            if (Subdivision != null)
                Subdivision.DrawGizmos(Polygon.GetVertices());
        }
    }
}
