﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacadeLayout : IDrawGizmos
{
    public FacadeLayout(BaseLayout baseLayout, InteriorLayout interiorLayout, float minWindowDistance, int maxwindows)
    {
        if (baseLayout != null && baseLayout.Polygon != null)
        {
            CreateWalls(baseLayout.Polygon, interiorLayout.Walls);
            FindIntersections(interiorLayout.Walls);
            BuildingFactory.CreateFacade(this, minWindowDistance, maxwindows);
            
        }

    }


    public Polygon2D Wall { get; private set; }
    public Polygon2D OutsideWall { get; private set; }
    public Polygon2D InsideWall { get; private set; }
    private RuleSet ruleSet;

    private void CreateWalls(Polygon2D basePoly, Polygon2D[] interiorWalls)
    {
        ruleSet = BuildRules.FacadeRuleSet;
        // Copy base wall
        Wall = new Polygon2D(EdgeType.External);
        foreach (Vertex2D vertex in basePoly.GetVertices())
        {
            Wall.Add(vertex);
        }
        Wall.Close();

        // intersections
        List<Edge2D> interiorEdges = new List<Edge2D>();
        foreach (Polygon2D interiorWall in interiorWalls)
        {
            interiorEdges.AddRange(interiorWall.GetEdges());
        }

        Edge2D[] outsideEdges = Wall.GetEdges();

        foreach (Edge2D outsideEdge in outsideEdges)
        {
            foreach (Edge2D interiorEdge in interiorEdges)
            {
                if (outsideEdge.Intersects(interiorEdge.Extend(.5f), out float t1, out float t2))
                {
                    Wall.Intersections.Add(outsideEdge.Value(t1));
                }
            }
        }

        // Offset center wall to outside and inside
        OutsideWall = Wall.Offset(ruleSet.WallThickness / 2, OffsetMethod.Ortho);
        InsideWall = Wall.Offset(-ruleSet.WallThickness / 2, OffsetMethod.Ortho);
    }


    private void FindIntersections(Polygon2D[] interiorWalls)
    {
        foreach (Polygon2D interiorWall in interiorWalls)
        {
            // Only intersections to inside wall
            interiorWall.IntersectionsTo(InsideWall, false, true);
        }
    }


    public void DrawGizmos(object data)
    {
        bool show = (bool)data;
        if (Wall != null && show)
        {
            //Wall.DrawGizmos(BuildRules.WallHeight-.5f);
            OutsideWall.DrawGizmos(ruleSet.WallHeight+.5f);
            InsideWall.DrawGizmos(ruleSet.WallHeight+.5f);
        }
    }
}
