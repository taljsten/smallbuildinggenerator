﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetSubdivision : IDrawGizmos
{

    private Polygon2D basePoly;
    private Polygon2D offsetPoly;
    private float basePosY = 0;
    private float offsetPosY = 0;
    public List<Triangle> Triangles { get; private set; }
    public List<Vector3> Vertices { get; private set; }
    public bool IsSubdivided { get; private set; } = false;
    private float offsetY;
    private float baseY;

    public OffsetSubdivision(Polygon2D basePoly, Polygon2D offsetPoly, float baseY, float offsetY)
    {
        this.basePoly = basePoly;
        this.offsetPoly = offsetPoly;
        this.offsetY = offsetY;
        this.baseY = baseY;
        Triangles = new List<Triangle>();
        Vertices = new List<Vector3>();
        IsSubdivided = Subdivide();
    }

    private bool Subdivide()
    {
        if (basePoly == null || offsetPoly == null)
            return false;
        if (!basePoly.IsClosed() || !offsetPoly.IsClosed())
            return false;

        Vertex2D[] baseVerts = basePoly.GetVertices();
        Vertex2D[] offsetVerts = offsetPoly.GetVertices();

        if (baseVerts.Length != offsetVerts.Length)
            return false;

        
        SubdivideSlant(baseVerts, offsetVerts);
        
        return true;

    }

    private void SubdivideSlant(Vertex2D[] baseVerts, Vertex2D[] offsetVerts)
    {
        int length = baseVerts.Length;

        for (int i = 0; i < length; i++)
        {
            Vertices.Add(new Vector3(baseVerts[i].position.x, baseY, baseVerts[i].position.y));
            Vertices.Add(new Vector3(baseVerts[i].position.x, baseY, baseVerts[i].position.y));
        }

        for (int i = 0; i < length; i++)
        {
            Vertices.Add(new Vector3(offsetVerts[i].position.x, offsetY, offsetVerts[i].position.y));
            Vertices.Add(new Vector3(offsetVerts[i].position.x, offsetY, offsetVerts[i].position.y));
        }
                
        // Odd indices
        for (int index = 1; index < length * 2; index+=2)
        {
            int t1 = index;
            int t2 = index + 1;
            int t3 = index + length * 2;
            int t4 = index + length * 2 + 1;
            

            if (index == length * 2 - 1)
            {
                t2 = 0;
                t4 = length * 2;
            }

            Triangles.Add(new Triangle(t1, t3, t2));
            Triangles.Add(new Triangle(t2, t3, t4));

        }
    }

    public void DrawGizmos(object data)
    {
        if (IsSubdivided)
        {

            Gizmos.color = Color.yellow; ;
            foreach (Triangle triangle in Triangles)
            {
                Vector3 a0 = Vertices[triangle.v0];
                Vector3 a1 = Vertices[triangle.v1];
                Vector3 a2 = Vertices[triangle.v2];

                Gizmos.DrawLine(a0, a1);
                Gizmos.DrawLine(a1, a2);
                Gizmos.DrawLine(a2, a0);
            } 
        }
    }
}
