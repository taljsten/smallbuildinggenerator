﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoofLayout : IDrawGizmos
{
    public Polygon2D Facade { get; private set; }
    
    public Polygon2D Offset { get; private set; }
    public PolygonSubdivision TopSubdivision { get; private set; }
    public OffsetSubdivision SlantSubdivision { get; private set; }
    public float Slant { get; private set; }

    //private float slant;


    public RoofLayout(BaseLayout baseLayout, float slant, float overhang)
    {
        Slant = slant;
        Facade = baseLayout.Polygon.Offset(overhang, OffsetMethod.Ortho);
        Offset = baseLayout.Polygon.Offset(-slant, OffsetMethod.Ortho);
        TopSubdivision = new PolygonSubdivision(Offset, false)
        {
            GizmoY = BuildRules.FacadeRuleSet.WallHeight + slant
        };

        SlantSubdivision = new OffsetSubdivision(Facade, Offset,
            BuildRules.FacadeRuleSet.WallHeight, BuildRules.FacadeRuleSet.WallHeight + slant);

    }




    public void DrawGizmos(object data)
    {
        bool show = (bool)data;

        if (show)
        {
            if (Facade != null)
                Facade.DrawGizmos(BuildRules.FacadeRuleSet.WallHeight);
            ////if (Subdivision != null)
            //    Subdivision.DrawGizmos(Polygon.GetVertices());
            if (Offset != null)
                Offset.DrawGizmos(TopSubdivision.GizmoY);

            if (TopSubdivision != null)
                TopSubdivision.DrawGizmos(Offset.GetVertices());

            if (SlantSubdivision != null)
                SlantSubdivision.DrawGizmos(null);
        }
    }

    
}
