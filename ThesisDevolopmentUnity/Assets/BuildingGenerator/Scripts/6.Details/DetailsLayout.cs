﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetailsLayout : IDrawGizmos
{
    private List<Window> windows;
    private List<DoorFrame> doorFrames;
    private List<DoorPanel> doorPanels;

    public DetailsLayout(Polygon2D facade, Polygon2D[] interiorWalls, DetailsManager detailsManager)
    {
        CreateWindows(facade, detailsManager);
        CreateDoorFrames(facade, interiorWalls, detailsManager);
        CreateDoorPanels(facade, interiorWalls, detailsManager);
    }

    private void CreateDoorPanels(Polygon2D facade, Polygon2D[] interiorWalls, DetailsManager detailsManager)
    {
        RuleSet ruleSet = BuildRules.DetailsRuleSet;
        doorPanels = new List<DoorPanel>();
        foreach (Edge2D edge in facade.GetEdges())
        {
            foreach (Feature feature in edge.GetFeatures())
            {
                if (feature.Type == FeatureType.Door)
                {
                    DoorPanel d = new DoorPanel(ruleSet, edge, feature, detailsManager, false);
                    doorPanels.Add(d);
                }
            }
        }

        ruleSet.WallThickness = BuildRules.InnerWallThickness;
        foreach (Polygon2D wall in interiorWalls)
        {
            foreach (Edge2D edge in wall.GetEdges())
            {
                foreach (Feature feature in edge.GetFeatures())
                {
                    if (feature.Type == FeatureType.Door)
                    {
                        DoorPanel d = new DoorPanel(ruleSet, edge, feature, detailsManager, true);
                        doorPanels.Add(d);
                    }
                }
            }
        }
    }

    private void CreateDoorFrames(Polygon2D facade, Polygon2D[] interiorWalls, DetailsManager detailsManager)
    {

        RuleSet ruleSet = BuildRules.DetailsRuleSet;
        doorFrames = new List<DoorFrame>();
        foreach (Edge2D edge in facade.GetEdges())
        {
            foreach (Feature feature in edge.GetFeatures())
            {
                if (feature.Type == FeatureType.Door)
                {
                    DoorFrame d = new DoorFrame(ruleSet, edge, feature, detailsManager, false);
                    doorFrames.Add(d);
                }
            }
        }

        ruleSet.WallThickness = BuildRules.InnerWallThickness;
        foreach (Polygon2D wall in interiorWalls)
        {
            foreach (Edge2D edge in wall.GetEdges())
            {
                foreach (Feature feature in edge.GetFeatures())
                {
                    if (feature.Type == FeatureType.Door)
                    {
                        DoorFrame d = new DoorFrame(ruleSet, edge, feature, detailsManager, true);
                        doorFrames.Add(d);
                    }
                }
            } 
        }
    }

    private void CreateWindows(Polygon2D facade, DetailsManager detailsManager)
    {
        RuleSet ruleSet = BuildRules.DetailsRuleSet;
        windows = new List<Window>();
        foreach (Edge2D edge in facade.GetEdges())
        {
            foreach (Feature feature in edge.GetFeatures())
            {
                if (feature.Type== FeatureType.Window)
                {
                    Window w = new Window(ruleSet, edge, feature, detailsManager);
                    windows.Add(w);
                }
            }
        }
    }

    public void DrawGizmos(object data)
    {
        bool drawGizmos = (bool)data;
        if (windows != null && drawGizmos)
        {
            foreach (Window window in windows)
            {
                window.DrawGizmos();
            }
        }
    }

    public List<MeshData> GetWindowsMeshData()
    {
        List<MeshData> windowsMeshData = new List<MeshData>();
        foreach (Window window in windows)
        {
            foreach (MeshData meshData in window.MeshData)
                windowsMeshData.Add(meshData);
        }
        return windowsMeshData;
    }

    public List<MeshData> GetDoorPanelsMeshData()
    {
        List<MeshData> doorPanelsData = new List<MeshData>();
        foreach (DoorPanel panel in doorPanels)
        {
            foreach (MeshData meshData in panel.MeshData)
                doorPanelsData.Add(meshData);
        }
        return doorPanelsData;
    }

    public List<MeshData> GetDoorFramesMeshData()
    {
        List<MeshData> doorFramesMeshData = new List<MeshData>();
        foreach (DoorFrame frame in doorFrames)
        {
            foreach (MeshData meshData in frame.MeshData)
                doorFramesMeshData.Add(meshData);
        }
        return doorFramesMeshData;
    }
}
