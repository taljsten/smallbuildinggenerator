﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DoorFrame 
{
	private readonly RuleSet ruleSet;
	//private readonly Edge2D wallEdge;
	//private readonly Feature feature;
	private readonly Vector3 orthoDir;
	private readonly Vector3 pos;
	private Quaternion rot;
	//private readonly DetailsManager detailsManager;
	public Polygon2D Frame { get; private set; } // Used as inner and outer and for edges
	public Polygon2D FootStep { get; private set; }
	public Polygon2D FootStepEdge { get; private set; }
	public List<MeshData> MeshData { get; private set; }

	public DoorFrame(RuleSet ruleSet, Edge2D wallEdge, Feature feature, DetailsManager detailsManager, bool centered)
	{
		this.ruleSet = ruleSet;
		//this.wallEdge = wallEdge;
		//this.feature = feature;
		//this.detailsManager = detailsManager;
		orthoDir = new Vector3(wallEdge.OrthoDirection.x, 0, wallEdge.OrthoDirection.y);
		Vector2 pos2d = wallEdge.Value(feature.Position);
		pos = new Vector3(pos2d.x, 0, pos2d.y) - orthoDir * ( detailsManager.EdgeHeight);
		if (centered)
			pos -= orthoDir * (ruleSet.WallThickness / 2);
		rot = Quaternion.AngleAxis(wallEdge.Direction.RealAngleTo(Vector2.right) * Mathf.Rad2Deg, Vector3.up);

		CreatePolys(detailsManager);
		Meshify(detailsManager);
	}

	
	private void CreatePolys(DetailsManager detailsManager)
	{
		Frame = new Polygon2D(EdgeType.External);
		FootStep = new Polygon2D(EdgeType.External);
		FootStepEdge = new Polygon2D(EdgeType.External);
		FramePoly(detailsManager);
		FootStepPoly(detailsManager);
	}

	private void FootStepPoly(DetailsManager detailsManager)
	{
		float thickness = ruleSet.WallThickness + detailsManager.EdgeHeight * 2;

		//Top
		Vector2 p0 = Frame.GetVertices()[0].position;
		Vector2 p1 = Frame.GetVertices()[5].position;
		Vector2 p2 = p1 + new Vector2(0,thickness);
		Vector2 p3 = p0 + new Vector2(0, thickness);
		FootStep.Add(new Vector2[]{p0,p1,p2,p3});
		FootStep.Close();

		Vector2 p4 = p1 + new Vector2(0, detailsManager.EdgeHeight);
		Vector2 p5 = p0 + new Vector2(0, detailsManager.EdgeHeight);
		FootStepEdge.Add(new Vector2[] { p0, p1, p4, p5 });
		FootStepEdge.Close();
	}

	private void FramePoly(DetailsManager detailsManager)
	{
		float lw = detailsManager.LiningWidth;
		float ft = detailsManager.FrameThickness;
		float iWidth = ruleSet.DoorSize.x - ft * 2;
		float iHeight = ruleSet.DoorSize.y - ft;
		float oWidth = ruleSet.DoorSize.x + lw * 2;
		float oHeight = ruleSet.DoorSize.y + lw;

		// Poly
		Vector2 p0 = new Vector2(iWidth / 2, 0);
		Vector2 p1 = new Vector2(oWidth / 2, 0);
		Vector2 p2 = p1 + new Vector2(0, oHeight);
		Vector2 p3 = p2 - new Vector2(oWidth, 0);
		Vector2 p4 = p3 - new Vector2(0, oHeight);
		Vector2 p5 = new Vector2(-iWidth / 2, 0);
		Vector2 p6 = p5 + new Vector2(0, iHeight);
		Vector2 p7 = p6 + new Vector2(iWidth, 0);
		Frame.Add(p0);
		Frame.Add(p1);
		Frame.Add(p2);
		Frame.Add(p3);
		Frame.Add(p4);
		Frame.Add(p5);
		Frame.Add(p6);
		Frame.Add(p7);
		Frame.Close();

	}

	private void Meshify(DetailsManager detailsManager)
	{
		MeshData = new List<MeshData>();
		MeshifyFrame(detailsManager);
		MeshifyEdge(detailsManager);
		MeshifyFootstep(detailsManager);
	}

	private void MeshifyFootstep(DetailsManager detailsManager)
	{
		Vector3[] verts = new Vector3[12];
		//List<Vertex2D> polyVerts = new List<Vertex2D>();
		//int index = 0;
		List<Vertex2D> polyverts = new List<Vertex2D>();
		polyverts.AddRange(FootStep.GetVertices());
		polyverts.AddRange(FootStepEdge.GetVertices());
		polyverts.AddRange(FootStepEdge.GetVertices());
		for (int i = 0; i < 4; i++)
		{
			verts[i] = new Vector3(polyverts[i].position.x, detailsManager.EdgeHeight, polyverts[i].position.y);
		}

		float zPos = ruleSet.WallThickness + detailsManager.EdgeHeight * 2;

		for (int i = 4; i < 8; i++)
		{
			verts[i] = new Vector3(polyverts[i].position.x, polyverts[i].position.y, 0);
		}

		for (int i = 8; i < 12; i++)
		{
			verts[i] = new Vector3(polyverts[i].position.x, polyverts[i].position.y, zPos);
		}

		List<Triangle> triangles = new List<Triangle>();
		PolygonSubdivision subDivStep = new PolygonSubdivision(FootStep, true);
		PolygonSubdivision subDivEdge1 = new PolygonSubdivision(FootStepEdge, true);
		PolygonSubdivision subDivEdge2 = new PolygonSubdivision(FootStepEdge, false);

		

		triangles.AddRange(subDivStep.Triangles);
		triangles.AddRange(subDivEdge1.GetTriangles(4));
		triangles.AddRange(subDivEdge2.GetTriangles(8));

		List<Vector2> uv = new List<Vector2>();		

		float scale = detailsManager.MaterialScale * (detailsManager.FacadeFrameTrimHeight / Frame.Size.y);
		Vector2 min = FootStep.Min * scale;
		Vector2 offset = new Vector2(0, detailsManager.FacadeFrameTrimPos - min.y);
		uv.AddRange(FootStep.GetUVs(scale, offset));
		uv.AddRange(FootStepEdge.GetUVs(scale, offset));
		uv.AddRange(FootStepEdge.GetUVs(scale, offset));



		MeshData footstepmesh = new MeshData(verts, uv.ToArray(), triangles.ToArray(), ruleSet.MaterialId, pos, rot); ;
		MeshData.Add(footstepmesh);
	}

	private void MeshifyEdge(DetailsManager detailsManager)
	{
		Vector3[] verts = new Vector3[24];
		//List<Vertex2D> polyVerts = new List<Vertex2D>();
		int index = 0;

		// Verts at z = 0
		foreach (Vertex2D vertex in Frame.GetVertices())
		{
			verts[index++] = new Vector3(vertex.position.x, vertex.position.y, 0);
			if (vertex.position.y > 0)
				verts[index++] = new Vector3(vertex.position.x, vertex.position.y, 0);
		}

		// verts at offset z=
		float zPos = ruleSet.WallThickness + detailsManager.EdgeHeight * 2;
		foreach (Vertex2D vertex in Frame.GetVertices())
		{
			verts[index++] = new Vector3(vertex.position.x, vertex.position.y, zPos);
			if (vertex.position.y > 0)
				verts[index++] = new Vector3(vertex.position.x, vertex.position.y, zPos);
		}

		
		// triangles
		List<Triangle> triangles = new List<Triangle>();
		// Vertical
		triangles.Add(new Triangle(1, 14, 13));
		triangles.Add(new Triangle(1, 2, 14));

		triangles.Add(new Triangle(5, 6, 18));
		triangles.Add(new Triangle(5, 18, 17));
		
		triangles.Add(new Triangle(0, 12, 11));
		triangles.Add(new Triangle(11, 12, 23));		

		triangles.Add(new Triangle(7, 20, 19));
		triangles.Add(new Triangle(7, 8, 20));

		// Horizontal
		triangles.Add(new Triangle(3, 16, 15));
		triangles.Add(new Triangle(3, 4, 16));

		triangles.Add(new Triangle(9, 10, 22));
		triangles.Add(new Triangle(9, 22, 21));


		Vector2[] uv = new Vector2[verts.Length];
		float scale = detailsManager.MaterialScale * (detailsManager.FacadeFrameTrimHeight / Frame.Size.y);
		Vector2 min = Frame.Min * scale;
		Vector2 offset = new Vector2(0, detailsManager.FacadeFrameTrimPos - min.y);
		for (int i = 0; i < uv.Length; i++)
		{
			// TODO: ändra ordningen när de lägg till
			// horisontella
			if (i==3 || i == 4 || i == 9 || i == 10 || i == 15 || i == 16 || i == 21 || i == 22)
			{
				uv[i] = new Vector2(verts[i].x, verts[i].z) * scale + offset;
			}
			// vertikala
			else
			{
				uv[i] = new Vector2(verts[i].y, verts[i].z) * scale + offset;
			}
		}

		MeshData edgeMesh = new MeshData(verts, uv.ToArray(), triangles.ToArray(), ruleSet.MaterialId, pos, rot); ;
		MeshData.Add(edgeMesh);
	}

	private void MeshifyFrame(DetailsManager detailsManager)
	{
		// Vertices
		Vector3[] verts = new Vector3[16];
		int index = 0;
		List<Vertex2D> polyVerts = new List<Vertex2D>();
		
		polyVerts.AddRange(Frame.GetVertices()); //Facade
		polyVerts.AddRange(Frame.GetVertices()); // Inside
		

		float zPos = ruleSet.WallThickness + detailsManager.EdgeHeight*2;
		for (int i = 0; i < polyVerts.Count; i++)
		{
			// 2 verts in 
			if (i < 8)
				verts[i] = new Vector3(polyVerts[i].position.x, polyVerts[i].position.y, 0);
			else
				verts[i] = new Vector3(polyVerts[i].position.x, polyVerts[i].position.y, zPos);
		}
		
		// Triangles
		Triangle[] triangles = new Triangle[12];
		index = 0;

		// Facade
		triangles[index++] = new Triangle(0, 2, 1);
		triangles[index++] = new Triangle(0, 7, 2);
		triangles[index++] = new Triangle(7, 3, 2);
		triangles[index++] = new Triangle(7, 6, 3);
		triangles[index++] = new Triangle(6, 4, 3);
		triangles[index++] = new Triangle(6, 5, 4);
		// inside
		triangles[index++] = new Triangle(8, 9, 10);
		triangles[index++] = new Triangle(8, 10, 15);
		triangles[index++] = new Triangle(15, 10, 11);
		triangles[index++] = new Triangle(15, 11, 14);
		triangles[index++] = new Triangle(14, 11, 12);
		triangles[index++] = new Triangle(14, 12, 13);

		// UV layout
		List<Vector2> uv = new List<Vector2>();
		float scale = detailsManager.MaterialScale * (detailsManager.FacadeFrameTrimHeight / Frame.Size.y);
		Vector2 min = Frame.Min * scale;
		Vector2 offset = new Vector2(0, detailsManager.FacadeFrameTrimPos - min.y);
		uv.AddRange(Frame.GetUVs(scale, offset));
		uv.AddRange(Frame.GetUVs(scale, offset));

		MeshData frameMesh = new MeshData(verts, uv.ToArray(), triangles, ruleSet.MaterialId, pos, rot); ;
		MeshData.Add(frameMesh);
	}
}
