﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPanel
{
    public Polygon2D Panel { get; set; }
    private readonly RuleSet ruleSet;
    private readonly Vector3 orthoDir;
    private readonly Vector3 pos;
    private readonly Quaternion rot;
	public List<MeshData> MeshData { get; private set; }

	public DoorPanel(RuleSet ruleSet, Edge2D wallEdge, Feature feature, DetailsManager detailsManager, bool centered)
    {
		this.ruleSet = ruleSet;
		//this.wallEdge = wallEdge;
		//this.feature = feature;
		//this.detailsManager = detailsManager;
		orthoDir = new Vector3(wallEdge.OrthoDirection.x, 0, wallEdge.OrthoDirection.y);
		Vector2 pos2d = wallEdge.Value(feature.Position);
		pos = new Vector3(pos2d.x, 0, pos2d.y) - orthoDir * (detailsManager.EdgeHeight + detailsManager.DoorPanelPosOffset) +
			Vector3.up * detailsManager.EdgeHeight;
		if (centered)
			pos -= orthoDir * (ruleSet.WallThickness / 2);
		rot = Quaternion.AngleAxis(wallEdge.Direction.RealAngleTo(Vector2.right) * Mathf.Rad2Deg, Vector3.up);

		CreatePolys(detailsManager);
		Meshify(detailsManager);
	}

	private void Meshify(DetailsManager detailsManager)
	{
		MeshData = new List<MeshData>();
		MeshifyPanel(detailsManager);
	}

	private void MeshifyPanel(DetailsManager detailsManager)
	{
		float thickness = detailsManager.DoorPanelThickness;
		MeshData panelMesh = Panel.Extrude(thickness, false, 
										detailsManager.DoorPanelMaterialScale, 
										detailsManager.DoorPanelTrimHeight, 
										detailsManager.DoorPanelTrimPos, 
										detailsManager.DoorPanelMaterialAngle*Mathf.Deg2Rad);
		panelMesh.MaterialId = ruleSet.MaterialId;
		panelMesh.Position = pos;
		panelMesh.Rotation = rot;
		MeshData.Add(panelMesh);
	}

	private void CreatePolys(DetailsManager detailsManager)
	{
		Panel = new Polygon2D(EdgeType.External);
		DoorPanelPoly(detailsManager);

	}

	private void DoorPanelPoly(DetailsManager detailsManager)
	{
		float ft = detailsManager.FrameThickness;
		float width = ruleSet.DoorSize.x - ft * 2;
		float height = ruleSet.DoorSize.y - ft - detailsManager.EdgeHeight;
		
		Panel.Add(new Vector2(width / 2, 0));
		Panel.Add(new Vector2(width / 2, height));
		Panel.Add(new Vector2(-width / 2, height));
		Panel.Add(new Vector2(-width / 2, 0));
		Panel.Close();
	}
}
