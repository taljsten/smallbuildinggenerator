﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
public class Window
{
    public Polygon2D FrameOuter { get; private set; }
    public Polygon2D FacadeCenter { get; private set; }
    public Polygon2D FrameInner { get; private set; }
    public Polygon2D Glass { get; private set; }

    //public Polygon2D FacadeEdge { get; private set; }       // Reused for both sides

    public List<MeshData> MeshData { get; private set; }
    private Vector3 pos;

    //private Vector3 dir;
    //private Edge2D edge;
    private Vector3 orthoDir;
    private Quaternion rot;
    private RuleSet ruleSet;

    public Window(RuleSet ruleSet, Edge2D wallEdge, Feature feature, /*, Vector3 posOnFacade , Vector3 direction, float liningWidth, float frameThickness, */DetailsManager detailsManager)
    {
        //dir = new Vector3(edge.Direction.x, 0, edge.Direction.y);
        //this.edge = edge;

        orthoDir = new Vector3(wallEdge.OrthoDirection.x, 0, wallEdge.OrthoDirection.y);
        Vector2 pos2d = wallEdge.Value(feature.Position);
        pos = new Vector3(pos2d.x, ruleSet.WindowStartHeight, pos2d.y) - orthoDir * (detailsManager.EdgeHeight) ;
        rot = Quaternion.AngleAxis(wallEdge.Direction.RealAngleTo(Vector2.right) * Mathf.Rad2Deg, Vector3.up);
        this.ruleSet = ruleSet;
        CreatePolys(detailsManager);
        Meshify(ruleSet.MaterialId, detailsManager);
    }

    private void CreatePolys(DetailsManager detailsManager)
    {

        FrameOuter = new Polygon2D(EdgeType.External);
        FacadeCenter = new Polygon2D(EdgeType.External);
        FrameInner = new Polygon2D(EdgeType.External);
        Glass = new Polygon2D(EdgeType.External);


        // Frame facade
        FacadePoly(detailsManager);
        GlassPoly(detailsManager, FacadeCenter);
    }

    private void GlassPoly(DetailsManager detailsManager, Polygon2D facadeCenter)
    {
        Glass = facadeCenter.Offset(0.05f, OffsetMethod.Ortho);
    }

    //private void FacadeEdgePoly(RuleSet ruleSet, DetailsManager detailsManager)
    //{
    //    FacadeEdge = FacadeOuter.Copy(EdgeType.External);
    //}

    private void FacadePoly(DetailsManager detailsManager)
    {
        float lw = detailsManager.LiningWidth;
        float ft = detailsManager.FrameThickness;

        float iWidth = ruleSet.WindowSize.x - ft * 2;
        float iHeight = ruleSet.WindowSize.y - ft * 2;
        float oWidth = ruleSet.WindowSize.x + lw * 2;
        float oHeight = ruleSet.WindowSize.y + lw * 2;
        Vector2 offset = new Vector2(0, -lw /*+ ft*/);

        Vector2 p0 = new Vector2(oWidth / 2, 0) + offset;
        Vector2 p1 = p0 + new Vector2(0, oHeight);
        Vector2 p2 = p1 - new Vector2(oWidth, 0);
        Vector2 p3 = p2 - new Vector2(0, oHeight);
        FrameOuter.Add(new Vertex2D(p0));
        FrameOuter.Add(new Vertex2D(p1));
        FrameOuter.Add(new Vertex2D(p2));
        FrameOuter.Add(new Vertex2D(p3));
        FrameOuter.Close();

        Vector2 c0 = new Vector2(ruleSet.WindowSize.x / 2, 0);
        Vector2 c1 = new Vector2(ruleSet.WindowSize.x / 2, ruleSet.WindowSize.y);
        Vector2 c2 = new Vector2(-ruleSet.WindowSize.x / 2, ruleSet.WindowSize.y);
        Vector2 c3 = new Vector2(-ruleSet.WindowSize.x / 2, 0);
        FacadeCenter.Add(new Vertex2D(c0));
        FacadeCenter.Add(new Vertex2D(c1));
        FacadeCenter.Add(new Vertex2D(c2));
        FacadeCenter.Add(new Vertex2D(c3));
        FacadeCenter.Close();
        FacadeCenter = FacadeCenter.Offset(detailsManager.FrameThickness * 2, OffsetMethod.Ortho);

        Vector2 p4 = new Vector2(iWidth / 2, ft + lw) + offset;
        Vector2 p5 = p4 + new Vector2(0, iHeight);
        Vector2 p6 = p5 - new Vector2(iWidth, 0);
        Vector2 p7 = p6 - new Vector2(0, iHeight);
        FrameInner.Add(new Vertex2D(p4));
        FrameInner.Add(new Vertex2D(p5));
        FrameInner.Add(new Vertex2D(p6));
        FrameInner.Add(new Vertex2D(p7));
        FrameInner.Close();
    }

    private void Meshify(int materialID, DetailsManager detailsManager)
    {
        float windowDepth = detailsManager.EdgeHeight * 2 + ruleSet.WallThickness;
        MeshData = new List<MeshData>();
        MeshifyFrame(FrameOuter, FrameInner, materialID, detailsManager, 0f, true);
        MeshifyEdge(detailsManager, materialID, detailsManager.EdgeHeight, 0f, FrameOuter, false, false);
        MeshifyEdge(detailsManager, materialID, windowDepth, 0f, FrameInner, true, false);
        MeshifyFrame(FacadeCenter, FrameInner, materialID, detailsManager, windowDepth, false);
        MeshifyEdge(detailsManager, materialID, detailsManager.EdgeHeight, windowDepth, FacadeCenter, true, true);
        MeshifyGlass(materialID, detailsManager, windowDepth, true );
        MeshifyGlass(materialID, detailsManager, windowDepth, false );


    }

    private void MeshifyGlass(int materialID, DetailsManager detailsManager, float windowDepth, bool reversed)
    {
        PolygonSubdivision glassSubDivision = new PolygonSubdivision(Glass, reversed);
        float zPos = windowDepth / 2 + detailsManager.GlassPositionOffset;


        if (reversed)
            zPos += detailsManager.GlassThickness / 2;
        else
            zPos -= detailsManager.GlassThickness / 2;

        Vector3[] verts = new Vector3[4];
        int index = 0;
        foreach (Vector2 p in Glass.VerticesToVectors())
        {
            verts[index++] = new Vector3(p.x, p.y, zPos);
        }

        List<Vector2> uv = new List<Vector2>();
        float scale = detailsManager.MaterialScale * (detailsManager.GlassTrimHeight / Glass.Size.y);

        Vector2 min = Glass.Min * scale;
        Vector2 offset = new Vector2(0, detailsManager.GlassTrimPos - min.y);
        uv.AddRange(Glass.GetUVs(scale, offset));

        MeshData glassMesh = new MeshData(verts, uv.ToArray(), glassSubDivision.Triangles.ToArray(), materialID, pos, rot);

        MeshData.Add(glassMesh);

    }

    private void MeshifyEdge(DetailsManager detailsManager, int materialID, float height, float offset, Polygon2D contour, bool reversed, bool reverseDirection)
    {
        float edgedir = 1;
        if (reverseDirection)
            edgedir = -1;
        List<Vector3> verts = new List<Vector3>();
        // Outer
        foreach (Vertex2D vertex in contour.GetVertices())
        {
            // Add two for sharp edges
            verts.Add(new Vector3(vertex.position.x, vertex.position.y, offset));
            verts.Add(new Vector3(vertex.position.x, vertex.position.y, offset));
        }

        // Facade
        foreach (Vertex2D vertex in contour.GetVertices())
        {
            // Add two for sharp edges
            verts.Add(new Vector3(vertex.position.x, vertex.position.y, offset + height * edgedir));
            verts.Add(new Vector3(vertex.position.x, vertex.position.y, offset + height * edgedir));
        }

       // int index = 0; // TODO: Remove later

        List<Triangle> triangles = new List<Triangle>();
        Vector2[] uv = new Vector2[16];

        for (int i = 1; i < 8 ; i+=2)
        {
            int i0 = i;
            int i1 = i+1;
            int i2 = i+8;
            int i3 = i+9;
            if (i1 == 8)
            {
                i1 = 0;
                i3 = 8;
            }
            if (reversed)
            {
                triangles.Add(new Triangle(i0, i3, i1));
                triangles.Add(new Triangle(i0, i2, i3));
            }
            else
            {
                triangles.Add(new Triangle(i0, i1, i3));
                triangles.Add(new Triangle(i0, i3, i2));
            }

            float width = Vector3.Distance(verts[i0], verts[i1]);

            float scale = detailsManager.MaterialScale * (detailsManager.FacadeFrameTrimHeight / height);

            uv[i0] = new Vector2(0, detailsManager.FacadeFrameTrimPos);
            uv[i1] = uv[i0] + new Vector2(width, 0) * scale;
            uv[i2] = uv[i0] + new Vector2(0, height) * scale;
            uv[i3] = uv[i0] + new Vector2(width, height) * scale;

        }




        MeshData edgeStrips = new MeshData(verts.ToArray(), uv, triangles.ToArray(), materialID, pos, rot);
        MeshData.Add(edgeStrips);


    }

    private void MeshifyFrame(Polygon2D outer, Polygon2D inner, int materialID, DetailsManager detailsManager, float zPos, bool reversed)
    {

        Vector3[] verts = new Vector3[8];
        int index = 0;
        List<Vertex2D> polyVerts = new List<Vertex2D>();
        polyVerts.AddRange(outer.GetVertices());
        polyVerts.AddRange(inner.GetVertices());

        foreach (Vertex2D vertex in polyVerts)
        {
            verts[index++] = new Vector3(vertex.position.x, vertex.position.y, zPos);   //new Vector3(0, vertex.position.y, 0) + dir * vertex.position.x + pos;
        }

        Triangle[] triangles = new Triangle[8];
        index = 0;
        for (int i = 0; i < 4; i++)
        {
            int i0 = i;
            int i1 = i + 1;
            int i2 = i + 4;
            int i3 = i + 5;
            if (i0 == 3)
            {
                i1 = 0;
                i3 = 4;
            }
            if (reversed)
            {
                triangles[index++] = new Triangle(i0, i3, i1);
                triangles[index++] = new Triangle(i0, i2, i3);
            }
            else
            {
                triangles[index++] = new Triangle(i0, i1, i3);
                triangles[index++] = new Triangle(i0, i3, i2);
            }
        }

        // Uv layout
        List<Vector2> uv = new List<Vector2>();
        float scale = detailsManager.MaterialScale * (detailsManager.FacadeFrameTrimHeight / FrameOuter.Size.y);
        Vector2 min = FrameOuter.Min * scale;
        Vector2 offset = new Vector2(0, detailsManager.FacadeFrameTrimPos - min.y);
        uv.AddRange(FrameOuter.GetUVs(scale, offset));
        uv.AddRange(FrameInner.GetUVs(scale, offset));

        MeshData frame = new MeshData(verts, uv.ToArray(), triangles, materialID, pos, rot);

        MeshData.Add(frame);
    }



    public void DrawGizmos()
    {
        if (MeshData != null)
        {
            foreach (MeshData meshData in MeshData)
            {
                meshData.DrawGizmos();
            }

        }

    }
}
