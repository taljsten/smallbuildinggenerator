﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshHierarchyBuilder
{
    private BuildingPart partPrefab;
    private Transform root;
    private bool renderFloor;
    private bool renderInterior; 
    private bool renderFacade;
    private bool renderRoof;
    private bool renderDetails;

    public MeshHierarchyBuilder(BuildingPart partPrefab, Transform root, bool renderFloor, bool renderInterior, bool renderFacade, bool renderRoof, bool renderDetails)
    {
        this.partPrefab = partPrefab;
        this.root = root;
        this.renderFloor = renderFloor;
        this.renderInterior = renderInterior;
        this.renderFacade = renderFacade;
        this.renderRoof = renderRoof;
        this.renderDetails = renderDetails;
        if (partPrefab != null && root !=null)
            CreateTree();
    }

    private void CreateTree()
    {
        BuildingPart baseOfbuilding = root.gameObject.GetComponentInChildren<BuildingPart>();
        
        if (baseOfbuilding != null)
            Object.DestroyImmediate(baseOfbuilding.gameObject);

        //Base
        BuildingPart buildingBase = Object.Instantiate(partPrefab);
        buildingBase.name = "Part: Base";
        buildingBase.transform.parent = root;
        buildingBase.transform.localPosition = Vector3.zero;
        buildingBase.transform.localRotation = Quaternion.identity;
        //buildingBase.Init(BuildingResources.Meshes["floor"], BuildingResources.Materials["matFloor"]);

        //Base
        if (renderFloor)
        {
            BuildingPart floor = Object.Instantiate(partPrefab);
            floor.name = "Part: Floor";
            floor.transform.parent = buildingBase.transform;
            floor.transform.localPosition = Vector3.zero;
            floor.transform.localRotation = Quaternion.identity;
            floor.Init(BuildingResources.Meshes["floor"].Mesh, BuildingResources.Meshes["floor"].Material); 
        }


        //Interior
        BuildingPart interior = Object.Instantiate(partPrefab);
        interior.name = "Part: Interior";
        interior.transform.parent = buildingBase.transform;
        interior.transform.localPosition = Vector3.zero;
        interior.transform.localRotation = Quaternion.identity;
        //interior.Init(BuildingResources.Meshes["floor"], BuildingResources.Materials["matFloor"]);

        //Inner walls       
        bool hasMeshdata = true;
        int index = 0;
        if (renderInterior)
        {
            while (hasMeshdata)
            {
                string key = "iWalls " + index;
                if (BuildingResources.Meshes.ContainsKey(key))
                {
                    BuildingPart innerWall = Object.Instantiate(partPrefab);
                    innerWall.name = "Part: InnerWall " + index;
                    innerWall.transform.parent = interior.transform;
                    innerWall.transform.localPosition = Vector3.zero;
                    innerWall.transform.localRotation = Quaternion.identity;
                    innerWall.Init(BuildingResources.Meshes[key].Mesh, BuildingResources.Meshes[key].Material /*BuildingResources.Materials[BuildRules.InteriorRuleSet.MaterialId]*/);
                    index++;
                }
                else
                    hasMeshdata = false;
            } 
        }

        //Ceiling
        BuildingPart ceilingbase = Object.Instantiate(partPrefab);
        ceilingbase.name = "Part: Ceiling";
        ceilingbase.transform.parent = buildingBase.transform;
        ceilingbase.transform.localPosition = new Vector3(0, BuildRules.InteriorRuleSet.WallHeight,0);
        ceilingbase.transform.localRotation = Quaternion.identity;
        //buildingBase.Init(BuildingResources.Meshes["floor"], BuildingResources.Materials["matFloor"]);

        //Ceiling
        if (renderFloor)
        {
            BuildingPart ceiling = Object.Instantiate(partPrefab);
            ceiling.name = "Part: Ceiling";
            ceiling.transform.parent = ceilingbase.transform;
            ceiling.transform.localPosition = Vector3.zero;
            ceiling.transform.localRotation = Quaternion.identity;
            ceiling.Init(BuildingResources.Meshes["ceiling"].Mesh, BuildingResources.Meshes["floor"].Material);
        }






        //Facade

        BuildingPart facadeParent = Object.Instantiate(partPrefab);
        facadeParent.name = "Part: Façade";
        facadeParent.transform.parent = buildingBase.transform;
        facadeParent.transform.localPosition = Vector3.zero;
        facadeParent.transform.localRotation = Quaternion.identity;
        //interior.Init(BuildingResources.Meshes["floor"], BuildingResources.Materials["matFloor"]);


        // Facade
        hasMeshdata = true;
        index = 0;
        if (renderFacade)
        {
            while (hasMeshdata)
            {
                string key = "Facade " + index;
                if (BuildingResources.Meshes.ContainsKey(key))
                {
                    BuildingPart facade = Object.Instantiate(partPrefab);
                    facade.name = "Part: Façade " + index;
                    facade.transform.parent = facadeParent.transform;
                    facade.transform.localPosition = Vector3.zero;
                    facade.transform.localRotation = Quaternion.identity;
                    facade.Init(BuildingResources.Meshes[key].Mesh, BuildingResources.Meshes[key].Material /*BuildingResources.Materials[BuildRules.FacadeRuleSet.MaterialId]*/);
                    index++;
                }
                else
                    hasMeshdata = false;
            } 
        }

        // Roof
        BuildingPart roofParent = Object.Instantiate(partPrefab);
        roofParent.name = "Part: Roof";
        roofParent.transform.parent = buildingBase.transform;
        roofParent.transform.localPosition = Vector3.zero;
        roofParent.transform.localRotation = Quaternion.identity;

        if (renderRoof)
        {
            BuildingPart roofTop = Object.Instantiate(partPrefab);
            roofTop.name = "Part: Roof Top";
            roofTop.transform.parent = roofParent.transform;
            roofTop.transform.localPosition = Vector3.zero;
            roofTop.transform.localRotation = Quaternion.identity;
            roofTop.Init(BuildingResources.Meshes["roofTop"].Mesh, BuildingResources.Meshes["roofTop"].Material);

            BuildingPart roofSlant = Object.Instantiate(partPrefab);
            roofSlant.name = "Part: Roof Slant";
            roofSlant.transform.parent = roofParent.transform;
            roofSlant.transform.localPosition = Vector3.zero;
            roofSlant.transform.localRotation = Quaternion.identity;
            roofSlant.Init(BuildingResources.Meshes["roofSlant"].Mesh, BuildingResources.Meshes["roofSlant"].Material);
        }

        // Details
        BuildingPart detailsParent = Object.Instantiate(partPrefab);
        detailsParent.name = "Part: Details";
        detailsParent.transform.parent = buildingBase.transform;
        detailsParent.transform.localPosition = Vector3.zero;
        detailsParent.transform.localRotation = Quaternion.identity;

        if (renderDetails)
        {
            hasMeshdata = true;
            index = 0;
            while (hasMeshdata)
            {
                string key = "window " + index;
                if (BuildingResources.Meshes.ContainsKey(key))
                {
                    BuildingPart window = Object.Instantiate(partPrefab);
                    window.name = "Part: Window " + index;
                    window.transform.parent = detailsParent.transform;
                    window.transform.localPosition = BuildingResources.Meshes[key].Position;
                    window.transform.localRotation = BuildingResources.Meshes[key].Rotation;
                    window.Init(BuildingResources.Meshes[key].Mesh, BuildingResources.Meshes[key].Material /*BuildingResources.Materials[BuildRules.FacadeRuleSet.MaterialId]*/);
                    index++;
                }
                else
                    hasMeshdata = false;
            }

            hasMeshdata = true;
            index = 0;
            while (hasMeshdata)
            {
                string key = "door " + index;
                if (BuildingResources.Meshes.ContainsKey(key))
                {
                    BuildingPart doorFrame = Object.Instantiate(partPrefab);
                    doorFrame.name = "Part: Door " + index;
                    doorFrame.transform.parent = detailsParent.transform;
                    doorFrame.transform.localPosition = BuildingResources.Meshes[key].Position;
                    doorFrame.transform.localRotation = BuildingResources.Meshes[key].Rotation;
                    doorFrame.Init(BuildingResources.Meshes[key].Mesh, BuildingResources.Meshes[key].Material /*BuildingResources.Materials[BuildRules.FacadeRuleSet.MaterialId]*/);
                    index++;
                }
                else
                    hasMeshdata = false;
            }

            hasMeshdata = true;
            index = 0;
            while (hasMeshdata)
            {
                string key = "doorpanel " + index;
                if (BuildingResources.Meshes.ContainsKey(key))
                {
                    BuildingPart doorPanel = Object.Instantiate(partPrefab);
                    doorPanel.name = "Part: Door panel " + index;
                    doorPanel.transform.parent = detailsParent.transform;
                    doorPanel.transform.localPosition = BuildingResources.Meshes[key].Position;
                    doorPanel.transform.localRotation = BuildingResources.Meshes[key].Rotation;
                    doorPanel.Init(BuildingResources.Meshes[key].Mesh, BuildingResources.Meshes[key].Material /*BuildingResources.Materials[BuildRules.FacadeRuleSet.MaterialId]*/);
                    index++;
                }
                else
                    hasMeshdata = false;
            }

        }
    }
}
