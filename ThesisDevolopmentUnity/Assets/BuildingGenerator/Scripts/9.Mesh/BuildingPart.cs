﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BuildingPart : MonoBehaviour
{
    
    [SerializeField] MeshRenderer meshRenderer;
    [SerializeField] MeshFilter meshFilter;
   

    public void Init(Mesh mesh, Material material)
    {
       
        meshFilter.mesh = mesh;
        meshRenderer.material = material;

    }


    
}
