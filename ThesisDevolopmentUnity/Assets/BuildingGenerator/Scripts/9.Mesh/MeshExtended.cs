﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public struct MeshExtended
{
    

    public MeshExtended(Mesh mesh, Material material)
    {
        Mesh = mesh;
        Material = material;
        Position = Vector3.zero;
        Rotation = Quaternion.identity;
    }

    public MeshExtended(Mesh mesh, Material material, Vector3 position, Quaternion rotation) : this(mesh, material)
    {
        Position = position;
        Rotation = rotation;
    }

    public Mesh Mesh { get; set; }
    public Material Material { get; set; }
    public Vector3 Position { get; set; }
    public Quaternion Rotation { get; set; }
}
