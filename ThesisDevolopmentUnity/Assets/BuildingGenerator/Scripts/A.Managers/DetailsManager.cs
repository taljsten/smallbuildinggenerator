﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DetailsManager
{
    [SerializeField] private Material detailsTrimMaterial;
    [SerializeField] private float facadeFrameTrimPos = 0;
    [SerializeField] private float facadeFrameTrimHeight = 0.25f;
    [SerializeField] private float glassTrimPos = 0;
    [SerializeField] private float glassTrimHeight = 0.25f;
    [SerializeField] private float liningWidth = 0.2f;
    [SerializeField] private float frameThickness = 0.05f;
    [SerializeField] private float facadeEdgeHeight = 0.1f;
    [SerializeField] private float materialScale = 1F;
    [SerializeField] private float glassThickness = 0.5f;
    [SerializeField] private float glassPositionOffset = 0.1f;
    [SerializeField] private float doorPanelThickness = .1f;
    [SerializeField] private float doorPanelPosOffset = .1f;
    [SerializeField] private float doorPanelTrimPos = 0;
    [SerializeField] private float doorPanelTrimHeight = 0.25f;
    [SerializeField] private float doorPanelMaterialAngle = 90;
    [SerializeField] private float doorPanelMaterialScale = 1;

    public Material DetailsTrimMaterial { get => detailsTrimMaterial; set => detailsTrimMaterial = value; }
    public bool RenderDetails { get; set; } = true;
    public bool RenderDetailGizmos { get; set; } = true;
    public float FacadeFrameTrimPos { get => facadeFrameTrimPos; set => facadeFrameTrimPos = value; }
    public float FacadeFrameTrimHeight { get => facadeFrameTrimHeight; set => facadeFrameTrimHeight = value; }
    public float LiningWidth { get => liningWidth; set => liningWidth = value; }
    public float FrameThickness { get => frameThickness; set => frameThickness = value; }
    public float MaterialScale { get => materialScale; set => materialScale = value; }
    public float EdgeHeight { get => facadeEdgeHeight; set => facadeEdgeHeight = value; }
    public float GlassThickness { get => glassThickness; set => glassThickness = value; }
    public float GlassTrimPos { get => glassTrimPos; set => glassTrimPos = value; }
    public float GlassTrimHeight { get => glassTrimHeight; set => glassTrimHeight = value; }
    public float GlassPositionOffset { get => glassPositionOffset; set => glassPositionOffset = value; }
    public float DoorPanelThickness { get => doorPanelThickness; set => doorPanelThickness = value; }
    public float DoorPanelPosOffset { get => doorPanelPosOffset; set => doorPanelPosOffset = value; }
    public float DoorPanelTrimPos { get => doorPanelTrimPos; set => doorPanelTrimPos = value; }
    public float DoorPanelTrimHeight { get => doorPanelTrimHeight; set => doorPanelTrimHeight = value; }
    public float DoorPanelMaterialAngle { get => doorPanelMaterialAngle; set => doorPanelMaterialAngle = value; }
    public float DoorPanelMaterialScale { get => doorPanelMaterialScale; set => doorPanelMaterialScale = value; }
}
