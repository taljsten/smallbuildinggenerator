﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

public class BuildingGizmos
{
	public BuildingGizmos()
	{

	}

	public void Draw(Polygon2D basePoly, PolygonSubdivision baseSubdivision, Polygon2D[] innerPolys, Transform transform, bool renderBaseGizmos, bool renderSubDivisionGizmos)
	{
        Gizmos.matrix = transform.localToWorldMatrix;
        Handles.matrix = transform.localToWorldMatrix;
        if (basePoly != null && renderBaseGizmos)
        {
            basePoly.DrawGizmos(null);

            Vertex2D[] vertices = basePoly.GetVertices();
            if (baseSubdivision != null && renderSubDivisionGizmos)
            {
                baseSubdivision.DrawGizmos(vertices);
            }

        }


        if (innerPolys != null )
        {
            foreach (Polygon2D wall in innerPolys)
            {
                if (wall != null)
                    wall.DrawGizmos(null);
            }
        }
    }

}