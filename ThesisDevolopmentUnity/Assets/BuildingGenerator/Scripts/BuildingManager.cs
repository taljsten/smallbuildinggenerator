﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    // Serializers
    // TODO: all props needs to go into serializable classes
    [SerializeField] private BuildingMetrics buildingMetrics;
    [SerializeField] private MaterialSettings materialSettings;
    [SerializeField] private Operation[] baseOperations;    
    [SerializeField] private WallOperation[] interiorOperations;
    [SerializeField] private Material floorMaterial;
    [SerializeField] private Material interiorTrimMaterial;
    [SerializeField] private Material facadeTrimMaterial;
    [SerializeField] private Material roofMaterial;    
    [SerializeField] private float facadeWallsMaterialScale = 1;
    [SerializeField] private float facadeWallsTrimPos = 0;
    [SerializeField] private float facadeWallsTrimHeight = 0.25f;
    [SerializeField] private float floorMaterialScale = 1;
    [SerializeField] private float innerWallsMaterialScale = 1;
    [SerializeField] private float innerWallsTrimPos = .25f;
    [SerializeField] private float innerWallsTrimHeight = .25f;
    [SerializeField] private BuildingPart partPrefab;
    [SerializeField] private int seed = 12345;
    [SerializeField] private float minDoorDistance = 1;
    [SerializeField] private float minWindowDistance = 1;
    [SerializeField] private int maxNoOfWindowsPerWall = 2;
    [SerializeField] private float roofSlantDistance = 0.1F;
    [SerializeField] private float roofOverhangDistance = 0.1F;
    [SerializeField] private float roofMaterialScale = 1;

    // Managers
    [SerializeField] private DetailsManager detailsManager;
    public DetailsManager DetailsManager { get => detailsManager; set => detailsManager = value; }

    // Setup
    public float FloorMaterialScale { get => floorMaterialScale; set => floorMaterialScale = value; }        
    public float InnerWallsMaterialScale { get => innerWallsMaterialScale; set => innerWallsMaterialScale = value; }
    public float InnerWallsTrimPos { get => innerWallsTrimPos; set => innerWallsTrimPos = value; }
    public float InnerWallsTrimHeight { get => innerWallsTrimHeight; set => innerWallsTrimHeight = value; }
    public float FacadeWallsMaterialScale { get => facadeWallsMaterialScale; set => facadeWallsMaterialScale = value; }
    public float FacadeWallsTrimPos { get => facadeWallsTrimPos; set => facadeWallsTrimPos = value; }
    public float FacadeWallsTrimHeight { get => facadeWallsTrimHeight; set => facadeWallsTrimHeight=value; } 
    public Material FloorMaterial { get => floorMaterial; set => floorMaterial = value; }
    public Material InteriorTrimMaterial { get => interiorTrimMaterial; set => interiorTrimMaterial = value; }
    public Material FacadeTrimMaterial { get => facadeTrimMaterial; set => facadeTrimMaterial = value; }
    public Material RoofMaterial { get => roofMaterial; set => roofMaterial = value; }
    
    public float RoofSlantDistance { get => roofSlantDistance; set => roofSlantDistance = value; }
    public float RoofOverhangDistance { get => roofOverhangDistance; set => roofOverhangDistance = value; }
    public float RoofMaterialScale { get => roofMaterialScale; set => roofMaterialScale = value; }
    public Transform TestPoint { get; set; }

    //public Texture2D TextureTest { get ; set; }
    public BuildingPart PartPrefab { get => partPrefab; set => partPrefab = value; }
    public int Seed { get => seed; set => seed = value; }
    public float MinDoorDistance { get => minDoorDistance; set => minDoorDistance = value; }
    public float MinWindowDistance { get => minWindowDistance; set => minWindowDistance = value; }

    // Generators and builders
    public BuildingData BuildingData { get; private set; }
    public BaseLayout BaseLayout { get; private set; }
    public InteriorLayout InteriorLayout { get; private set; }
    public CeilingLayout CeilingLayout { get; private set; }
    public FacadeLayout FacadeLayout { get; private set; }
    public RoofLayout RoofLayout { get; private set; }
    public DetailsLayout DetailsLayout { get; private set; }
    private MeshHierarchyBuilder meshHierarchy;

    // Rendering
    public bool RenderFloor { get; set; } = true;
    public bool RenderInterior { get; set; } = true;
    public bool RenderFacade { get; set; } = true;
    public bool RenderRoof { get; set; } = true;
    
    

    // Gizmos
    public bool RenderBaseGizmos { get; set; } = true;
    public bool RenderInteriorGizmos { get; set; } = true;
    public bool RenderFacadeGizmos { get; set; } = true;
    public bool RenderRoofGizmos { get; set; } = true;
    
    
    public int MaxNoOfWindowsPerWall { get => maxNoOfWindowsPerWall; set => maxNoOfWindowsPerWall = value; }
    public BuildingMetrics BuildingMetrics { get => buildingMetrics; set => buildingMetrics = value; }
    public MaterialSettings MaterialSettings { get => materialSettings; set => materialSettings = value; }

    private BuildingGizmos buildingGizmos;

    // Statistics
    public float GenerationTime { get; set; }


    /// <summary>
    /// Called from Editor, when input data is changed
    /// </summary>
    public void Generate()
    {
        GenerationTime = Time.realtimeSinceStartup;

        for (int i = 0; i < 1; i++)
        {
            if (!SanityOk())
                return;

            if (baseOperations == null)
            {
                baseOperations = new Operation[1];
                baseOperations[0] = new Operation(0, 0);
            }

            if (interiorOperations == null)
            {
                interiorOperations = new WallOperation[1];
                interiorOperations[0] = new WallOperation
                {
                    angle = 0,
                    distance = 0.5f,
                    edgeIndex = 0
                };
            }

            UnityEngine.Random.InitState(Seed);
            BuildRules.FloorMaterialScale = FloorMaterialScale;
            BuildRules.InnerWallsTrimHeight = InnerWallsTrimHeight;
            BuildRules.InnerWallsMaterialScale = InnerWallsMaterialScale;
            BuildRules.InnerWallsTrimPos = InnerWallsTrimPos;

            BuildRules.OuterWallsTrimHeight = FacadeWallsTrimHeight;
            BuildRules.OuterWallsMaterialScale = FacadeWallsMaterialScale;
            BuildRules.OuterWallsTrimPos = FacadeWallsTrimPos;

            BuildRules.RoofMaterialScale = RoofMaterialScale;

            BuildingData = new BuildingData(/*FloorMaterialScale, InnerWallsMaterialScale, InnerWallsTrimPos*/);                        // Init data

            BaseLayout = new BaseLayout(baseOperations);                                                    // Build base 
            BuildingData.Add(BaseLayout);                                                                   // Base           
            InteriorLayout = new InteriorLayout(interiorOperations, BaseLayout.Polygon, MinDoorDistance);    // Interior
            BuildingData.Add(InteriorLayout);
            CeilingLayout = new CeilingLayout(BaseLayout);
            BuildingData.Add(CeilingLayout);
            FacadeLayout = new FacadeLayout(BaseLayout, InteriorLayout, MinWindowDistance, MaxNoOfWindowsPerWall);
            BuildingData.Add(FacadeLayout);
            RoofLayout = new RoofLayout(BaseLayout, RoofSlantDistance, roofOverhangDistance);
            BuildingData.Add(RoofLayout, BaseLayout.Angles);
            DetailsLayout = new DetailsLayout(FacadeLayout.OutsideWall, InteriorLayout.Walls, detailsManager);
            BuildingData.Add(DetailsLayout);


            AddToResources();                                                   // Add stuff to resources

            // Start building mesh
            meshHierarchy = new MeshHierarchyBuilder(PartPrefab, transform, RenderFloor, RenderInterior, RenderFacade, RenderRoof, detailsManager.RenderDetails);    // Build hierarchy
            buildingGizmos = new BuildingGizmos();                              // Gizmos

        }
        GenerationTime = Time.realtimeSinceStartup - GenerationTime;
        Debug.Log(GenerationTime.ToString("0.0000") + " seconds");

    }

    public bool SanityOk()
    {
        if (FloorMaterial == null)
            return false;
        if (FacadeTrimMaterial == null)
            return false;
        if (InteriorTrimMaterial == null)
            return false;

        return true;
    }

    private void AddToResources()
    {
        BuildingResources.Init();
        
        if (FloorMaterial != null)
            BuildingResources.AddMaterial(/*"matFloor",*/ FloorMaterial);               //0
        if (InteriorTrimMaterial != null)
            BuildingResources.AddMaterial(/*"matInteriorTrim",*/ InteriorTrimMaterial); //1
        if (FacadeTrimMaterial != null)
            BuildingResources.AddMaterial(/*"matFacadeTrim",*/ FacadeTrimMaterial);     //2
        if (RoofMaterial != null)
            BuildingResources.AddMaterial(/*"matFacadeTrim",*/ RoofMaterial);     //3
        if (detailsManager.DetailsTrimMaterial != null)
            BuildingResources.AddMaterial(/*"matFacadeTrim",*/detailsManager.DetailsTrimMaterial);     //4


        BuildingResources.PopulateMeshes(BuildingData);
    }

    void OnDrawGizmos()
    {
        Handles.matrix = transform.localToWorldMatrix;
        Gizmos.matrix = transform.localToWorldMatrix;


        if (BaseLayout != null)
            BaseLayout.DrawGizmos(RenderBaseGizmos);
        if (InteriorLayout != null)
            InteriorLayout.DrawGizmos(RenderInteriorGizmos);
        if (CeilingLayout != null)
            CeilingLayout.DrawGizmos(RenderInteriorGizmos);
        if (FacadeLayout != null)
            FacadeLayout.DrawGizmos(RenderFacadeGizmos);
        if (RoofLayout != null)
            RoofLayout.DrawGizmos(RenderRoofGizmos);
        if (DetailsLayout != null)
            DetailsLayout.DrawGizmos(detailsManager.RenderDetailGizmos);

    }
}
