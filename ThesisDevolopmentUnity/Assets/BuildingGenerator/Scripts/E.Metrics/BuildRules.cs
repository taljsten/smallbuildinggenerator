﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class BuildRules
{
    public static float InnerWallThickness { get; set; } = 0.3f;
    public static float OuterWallThickness { get; set; } = 0.5f;
    public static float WallHeight { get; set; } = 3.2f;
    //public static float DoorHeight { get; set; } = 2.2f;
    //public static float DoorWidth { get; set; } = 1.2f;
    public static Vector2 DoorSize { get; set; } = new Vector2(1f, 2.2f);
    public static Vector2 WindowsSize { get; set; } = new Vector2(.8f, 1.5f);
    public static float WindowStartHeight { get; set; } = .7f;
    public static float InnerWallsTrimPos { get; set; } = 0;
    public static float InnerWallsTrimHeight { get; set; } = 0.25f;
    public static float InnerWallsMaterialScale { get; set; } = 1;
    public static float OuterWallsTrimPos { get; set; } = 0;
    public static float OuterWallsTrimHeight { get; set; } = 0.25f;
    public static float OuterWallsMaterialScale { get; set; } = 1;
    public static float FloorMaterialScale { get; set; } = 1;
    public static float RoofMaterialScale { get; set; } = 1;
    


    public static RuleSet FloorRuleSet
    {
        get
        {
            return new RuleSet()
            {
                WallThickness = InnerWallThickness,
                WallHeight = WallHeight,
                DoorSize = DoorSize,
                WindowSize = WindowsSize,
                WindowStartHeight = WindowStartHeight,
                TrimPos = InnerWallsTrimPos,
                TrimHeight = InnerWallsTrimHeight,
                MaterialScale = FloorMaterialScale,
                MaterialId = 0
            };
        }
    }

    public static RuleSet InteriorRuleSet
    {
        get
        {
            return new RuleSet()
            {
                WallThickness = InnerWallThickness,
                WallHeight = WallHeight,
                DoorSize = DoorSize,
                WindowSize = WindowsSize,
                WindowStartHeight = WindowStartHeight,
                TrimPos = InnerWallsTrimPos,
                TrimHeight = InnerWallsTrimHeight,
                MaterialScale = InnerWallsMaterialScale,
                MaterialId = 1
            };
        }
    }

    public static RuleSet FacadeRuleSet
    {
        get
        {
            return new RuleSet()
            {
                WallThickness = OuterWallThickness,
                WallHeight = WallHeight,
                DoorSize = DoorSize,
                WindowSize = WindowsSize,
                WindowStartHeight = WindowStartHeight,
                TrimPos = OuterWallsTrimPos,
                TrimHeight = OuterWallsTrimHeight,
                MaterialScale = OuterWallsMaterialScale,
                MaterialId = 2
            };
        }
    }

    public static RuleSet RoofRuleSet
    {
        get
        {
            return new RuleSet()
            {
                WallThickness = InnerWallThickness,
                WallHeight = WallHeight,
                DoorSize = DoorSize,
                WindowSize = WindowsSize,
                WindowStartHeight = WindowStartHeight,
                TrimPos = OuterWallsTrimPos,
                TrimHeight = OuterWallsTrimHeight,
                MaterialScale = RoofMaterialScale,
                MaterialId = 3
            };
        }
    }

    public static RuleSet DetailsRuleSet
    {
        get
        {
            return new RuleSet()
            {
                WallThickness = OuterWallThickness,
                WallHeight = WallHeight,
                DoorSize = DoorSize,
                WindowSize = WindowsSize,
                WindowStartHeight = WindowStartHeight,
                TrimPos = OuterWallsTrimPos,
                TrimHeight = OuterWallsTrimHeight,
                MaterialScale = RoofMaterialScale,
                MaterialId = 4
            };
        }
    }
}
