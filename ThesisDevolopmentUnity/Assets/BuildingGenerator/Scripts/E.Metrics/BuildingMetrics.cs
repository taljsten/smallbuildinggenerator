﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildingMetrics 
{
    // Walls
    [SerializeField] float innerWallThickness = 0.3f;
    [SerializeField] float outerWallrhickness = 0.5f;
    [SerializeField] float wallHeight = 3.2f;
    public float InnerWallThickness { get => innerWallThickness; set => innerWallThickness = value; }
    public float OuterWallrhickness { get => outerWallrhickness; set => outerWallrhickness = value; }
    public float WallHeight { get => wallHeight; set => wallHeight = value; }
    

    // Roof
    [SerializeField] float roofSlantDistance = 1f;
    [SerializeField] float roofOverhangDistance = 0.3f;
    public float RoofSlantDistance { get => roofSlantDistance; set => roofSlantDistance = value; }
    public float RoofOverhangDistance { get => roofOverhangDistance; set => roofOverhangDistance = value; }
    

    // Windows
    [SerializeField] Vector2 windowSize = new Vector2(.8f, 1.5f);
    [SerializeField] float windowStartHeight = .7f;
    [SerializeField] int waxNoOfWindowsPerWall = 3;
    [SerializeField] float minimumWindowClearence = 1;
    [SerializeField] float windowLiningWidth = .2f;
    [SerializeField] float windowFrameThickness = .02f;
    [SerializeField] float windowEdgeHeight = 0.05f;
    [SerializeField] float glassThickness = 0.2f;
    public Vector2 WindowSize { get => windowSize; set => windowSize = value; }
    public float WindowStartHeight { get => windowStartHeight; set => windowStartHeight = value; }
    public int WaxNoOfWindowsPerWall { get => waxNoOfWindowsPerWall; set => waxNoOfWindowsPerWall = value; }
    public float MinimumWindowClearence { get => minimumWindowClearence; set => minimumWindowClearence = value; }
    public float WindowLiningWidth { get => windowLiningWidth; set => windowLiningWidth = value; }
    public float WindowFrameThickness { get => windowFrameThickness; set => windowFrameThickness = value; }
    public float WindowEdgeHeight { get => windowEdgeHeight; set => windowEdgeHeight = value; }
    public float GlassThickness { get => glassThickness; set => glassThickness = value; }
    

    // Doors
    [SerializeField] Vector2 doorSize = new Vector2(1.2f, 2.2f);
    [SerializeField] float minimumDoorClearence = 1;
    public Vector2 DoorSize { get => doorSize; set => doorSize = value; }
    public float MinimumDoorClearence { get => minimumDoorClearence; set => minimumDoorClearence = value; }


}
