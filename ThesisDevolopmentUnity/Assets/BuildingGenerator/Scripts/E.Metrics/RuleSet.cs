﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public struct RuleSet
{
    public float WallThickness { get; set; }
    public float WallHeight { get; set; }
    public Vector2 DoorSize { get; set; }
    public Vector2 WindowSize { get; set; }
    public float WindowStartHeight { get; set; }
    public float TrimPos { get; set; }
    public float TrimHeight { get; set; }
    public float MaterialScale { get; set; }
    public int MaterialId { get; set; }
    
}
