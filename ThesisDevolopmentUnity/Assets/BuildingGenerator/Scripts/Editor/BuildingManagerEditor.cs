﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEditorInternal;
using System;

[CustomEditor(typeof(BuildingManager))]
public class BuildingManagerEditor:Editor
{
    BuildingManager buildingManager;
    private GUIStyle header;
    private GUIStyle section1;
    private GUIStyle section2;
    private GUIStyle message;
    private GUIStyle error;
    private SerializedProperty operations;

    private ReorderableList opStack;
    private ReorderableList wallStack;
    private bool showRawData = true;
    private bool showBaseLayout = true;
    //private bool showSubdiv = true;
    private bool showInteriorLayout = true;
    private bool showSetup = true;
    private bool showFacade = true;
    private bool showRoof = true;
    private bool showRender = true;
    private bool showDetails = true;

    void OnEnable()
    {
        buildingManager = (BuildingManager)target;
        buildingManager.Generate();

        header = new GUIStyle();
        header.fontSize = 25;
        header.fontStyle = FontStyle.Bold;

        section1 = new GUIStyle();
        section1.fontSize = 15;
        section1.fontStyle = FontStyle.Bold;

        section2 = new GUIStyle();
        section2.fontSize = 13;
        section2.fontStyle = FontStyle.Bold;

        message = new GUIStyle();
        message.fontSize = 12;
        message.fontStyle = FontStyle.Bold;
        message.normal.textColor = Color.black;

        error = new GUIStyle();
        error.fontSize = 12;
        error.fontStyle = FontStyle.Bold;
        error.normal.textColor = Color.red;

        opStack = new ReorderableList(serializedObject, serializedObject.FindProperty("baseOperations"), true, true, true, true);
        opStack.drawHeaderCallback += DrawBaseListHeader;
        opStack.drawElementCallback += DrawListElementBase;
        wallStack = new ReorderableList(serializedObject, serializedObject.FindProperty("interiorOperations"), true, true, true, true);
        wallStack.drawHeaderCallback += DrawWallListHeader;
        wallStack.drawElementCallback += DrawListElementWalls;



        //operations = serializedObject.FindProperty("operations");

    }



    private void DrawListElementBase(Rect rect, int index, bool isActive, bool isFocused)
    {
        float height = EditorGUIUtility.singleLineHeight;

        SerializedProperty element = opStack.serializedProperty.GetArrayElementAtIndex(index);
        SerializedProperty dist = element.FindPropertyRelative("distance");
        SerializedProperty angle = element.FindPropertyRelative("angle");

        EditorGUI.LabelField(new Rect(rect.x, rect.y, 50, height), "Angle");
        EditorGUI.PropertyField(new Rect(rect.x + 44, rect.y, 50, height), angle, GUIContent.none);

        EditorGUI.LabelField(new Rect(rect.x + 125, rect.y, 75, height), "Distance");
        EditorGUI.PropertyField(new Rect(rect.x + 185, rect.y, 50, height), dist, GUIContent.none);

    }

    private void DrawListElementWalls(Rect rect, int index, bool isActive, bool isFocused)
    {
        float height = EditorGUIUtility.singleLineHeight;

        SerializedProperty element = wallStack.serializedProperty.GetArrayElementAtIndex(index);
        //SerializedProperty dist = element.FindPropertyRelative("distance");
        SerializedProperty angle = element.FindPropertyRelative("angle");
        //SerializedProperty newWall = element.FindPropertyRelative("newWall");
        SerializedProperty edgeIndex = element.FindPropertyRelative("edgeIndex");
        SerializedProperty edgeDist = element.FindPropertyRelative("distance");

        //EditorGUI.LabelField(new Rect(rect.x, rect.y, 30, height), "New");
        //newWall.boolValue = EditorGUI.Toggle(new Rect(rect.x + 30, rect.y, 25, height), newWall.boolValue);

        EditorGUI.LabelField(new Rect(rect.x, rect.y, 75, height), "Edge index");
        EditorGUI.PropertyField(new Rect(rect.x + 75, rect.y, 50, height), edgeIndex, GUIContent.none);

        EditorGUI.LabelField(new Rect(rect.x + 150, rect.y, 75, height), "Edge dist.");
        EditorGUI.PropertyField(new Rect(rect.x + 215, rect.y, 50, height), edgeDist, GUIContent.none);

        EditorGUI.LabelField(new Rect(rect.x + 290, rect.y, 50, height), "Angle");
        EditorGUI.PropertyField(new Rect(rect.x + 335, rect.y, 50, height), angle, GUIContent.none);

        //if (newWall.boolValue)
        //{
        //    EditorGUI.LabelField(new Rect(rect.x + 55, rect.y, 50, height), "Edge");
        //    EditorGUI.PropertyField(new Rect(rect.x + 95, rect.y, 100, height), edgeIndex, GUIContent.none);

        //    EditorGUI.LabelField(new Rect(rect.x + 215, rect.y, 75, height), "Distance");
        //    EditorGUI.PropertyField(new Rect(rect.x + 270, rect.y, 100, height), edgeDist, GUIContent.none);
        //}
        //else
        //{
        //    EditorGUI.LabelField(new Rect(rect.x + 55, rect.y, 50, height), "Angle");
        //    EditorGUI.PropertyField(new Rect(rect.x + 95, rect.y, 100, height), angle, GUIContent.none);

        //    EditorGUI.LabelField(new Rect(rect.x + 215, rect.y, 75, height), "Distance");
        //    EditorGUI.PropertyField(new Rect(rect.x + 270, rect.y, 100, height), dist, GUIContent.none);
        //}

    }

    private void DrawBaseListHeader(Rect rect)
    {
        GUI.Label(rect, "Base Operations. First entry is start pos and angle");
    }

    private void DrawWallListHeader(Rect rect)
    {
        GUI.Label(rect, "Wall Operations");
    }



    public override void OnInspectorGUI()
    {
        showRawData = EditorGUILayout.BeginFoldoutHeaderGroup(showRawData, "RawData");
        if (showRawData)
        {
            EditorGUILayout.LabelField("Raw data", section1, GUILayout.Height(30));
            base.OnInspectorGUI();
           
        }
        GUILayout.Space(30);
        EditorGUILayout.EndFoldoutHeaderGroup();
        //if (Application.isPlaying)
        //    return;

        //float height = 12;
        serializedObject.Update();
        //houseManager = (HouseManager)target;
        //base.OnInspectorGUI();

        EditorGUILayout.LabelField("Small Building Generator", header, GUILayout.Height(40));

        EditorGUILayout.LabelField("version 0.1 ", section1, GUILayout.Height(30));
        //if (houseManager.IsInvalid())
        //{

        //    EditorGUILayout.LabelField("Invalid: To many concave corners", error, GUILayout.Height(25));
        //}
        //else
        //{

        //    EditorGUILayout.LabelField("Calculation ok", message, GUILayout.Height(25));
        //}


        //EditorGUILayout.EndHorizontal();

        //operations = serializedObject.FindProperty("operations");

        EditorGUI.BeginChangeCheck();

        showSetup = EditorGUILayout.BeginFoldoutHeaderGroup(showSetup, "Setup");
        if (showSetup)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Seed");
            buildingManager.Seed = EditorGUILayout.IntField(buildingManager.Seed);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Part prefab");
            buildingManager.PartPrefab = (BuildingPart)EditorGUILayout.ObjectField(buildingManager.PartPrefab, typeof(BuildingPart), true);
            EditorGUILayout.EndHorizontal();

        }
        EditorGUILayout.EndFoldoutHeaderGroup();


        showRender = EditorGUILayout.BeginFoldoutHeaderGroup(showRender, "Render Settings");
        if (showRender)
        {
            buildingManager.RenderFloor = EditorGUILayout.ToggleLeft("Render Floor Mesh", buildingManager.RenderFloor);
            buildingManager.RenderInterior = EditorGUILayout.ToggleLeft("Render Interior Mesh(es)", buildingManager.RenderInterior);
            buildingManager.RenderFacade = EditorGUILayout.ToggleLeft("Render Façade Mesh", buildingManager.RenderFacade);
            buildingManager.RenderRoof = EditorGUILayout.ToggleLeft("Render Roof Mesh", buildingManager.RenderRoof);
            buildingManager.DetailsManager.RenderDetails = EditorGUILayout.ToggleLeft("Render Details Mesh(es)", buildingManager.DetailsManager.RenderDetails);


        }
        EditorGUILayout.EndFoldoutHeaderGroup();


        if (buildingManager.SanityOk())
        {



            showBaseLayout = EditorGUILayout.BeginFoldoutHeaderGroup(showBaseLayout, "Base Layout");
            if (showBaseLayout)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Floor material");
                buildingManager.FloorMaterial = (Material)EditorGUILayout.ObjectField(buildingManager.FloorMaterial, typeof(Material), true);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Floor material scale");
                buildingManager.FloorMaterialScale = EditorGUILayout.Slider(buildingManager.FloorMaterialScale, 0.1f, 10);
                EditorGUILayout.EndHorizontal();

                if (opStack != null)
                    opStack.DoLayoutList();

                if (buildingManager.BaseLayout != null && buildingManager.BaseLayout.Polygon != null)
                {
                    int c = buildingManager.BaseLayout.Polygon.NumConcaveCorners;
                    int i = buildingManager.BaseLayout.Polygon.Intersections.Count;
                    int o = buildingManager.BaseLayout.Polygon.NumberOverlappedVertices;
                    int s = buildingManager.BaseLayout.Polygon.NumberSmallAngles;
                    Vector2 size = buildingManager.BaseLayout.Polygon.Size;

                    EditorGUILayout.LabelField("Concave corners = " + c, (c > 1) ? error : message);
                    EditorGUILayout.LabelField("Intersections = " + i, (i != 0) ? error : message);
                    EditorGUILayout.LabelField("Overlapped vertices = " + o, (o != 0) ? error : message);
                    EditorGUILayout.LabelField("Small angles = " + s, (s != 0) ? error : message);
                    EditorGUILayout.LabelField("Size = " + size, message);
                    EditorGUILayout.LabelField("Number of triangles = " + buildingManager.BaseLayout.Subdivision.Triangles.Count);
                    buildingManager.RenderBaseGizmos = EditorGUILayout.ToggleLeft("Base Gizmos", buildingManager.RenderBaseGizmos);


                    if (buildingManager.BaseLayout.Polygon.IsUndefined || c > 1 || i != 0 || o != 0 || s != 0)
                        EditorGUILayout.LabelField("Error: Base is undefined", error); 
                }
                //EditorGUILayout.Space(25);

            }
            EditorGUILayout.EndFoldoutHeaderGroup();





            showInteriorLayout = EditorGUILayout.BeginFoldoutHeaderGroup(showInteriorLayout, "Interior Layout");
            if (showInteriorLayout)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Interior trim material");
                buildingManager.InteriorTrimMaterial = (Material)EditorGUILayout.ObjectField(buildingManager.InteriorTrimMaterial, typeof(Material), true);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Inner walls trim position");
                buildingManager.InnerWallsTrimPos = EditorGUILayout.Slider(buildingManager.InnerWallsTrimPos, 0, 1);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Inner walls trim height");
                buildingManager.InnerWallsTrimHeight = EditorGUILayout.Slider(buildingManager.InnerWallsTrimHeight, 0, 1);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Inner walls material scale");
                buildingManager.InnerWallsMaterialScale = EditorGUILayout.Slider(buildingManager.InnerWallsMaterialScale, 0.1f, 1);
                EditorGUILayout.EndHorizontal();

                if (wallStack != null)
                    wallStack.DoLayoutList();
                buildingManager.RenderInteriorGizmos = EditorGUILayout.ToggleLeft("Interior Gizmos", buildingManager.RenderInteriorGizmos);
            }
            EditorGUILayout.EndFoldoutHeaderGroup();

            showFacade = EditorGUILayout.BeginFoldoutHeaderGroup(showFacade, "Façade Layout");
            if (showFacade)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Façade trim material");
                buildingManager.FacadeTrimMaterial = (Material)EditorGUILayout.ObjectField(buildingManager.FacadeTrimMaterial, typeof(Material), true);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Façade walls trim position");
                buildingManager.FacadeWallsTrimPos = EditorGUILayout.Slider(buildingManager.FacadeWallsTrimPos, 0, 1);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Façade walls trim height");
                buildingManager.FacadeWallsTrimHeight = EditorGUILayout.Slider(buildingManager.FacadeWallsTrimHeight, 0, 1);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Façade walls material scale");
                buildingManager.FacadeWallsMaterialScale = EditorGUILayout.Slider(buildingManager.FacadeWallsMaterialScale, 0.1f, 1);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Min door distance from corner, door or window");
                buildingManager.MinDoorDistance = EditorGUILayout.Slider(buildingManager.MinDoorDistance, 0.5f, 3f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Min window distance from corner, door or window");
                buildingManager.MinWindowDistance = EditorGUILayout.Slider(buildingManager.MinWindowDistance, 0.5f, 5f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Max number of windows per wall");
                buildingManager.MaxNoOfWindowsPerWall = EditorGUILayout.IntSlider(buildingManager.MaxNoOfWindowsPerWall, 1, 5);
                EditorGUILayout.EndHorizontal();

                buildingManager.RenderFacadeGizmos = EditorGUILayout.ToggleLeft("Façade Gizmos", buildingManager.RenderFacadeGizmos);
            }
            EditorGUILayout.EndFoldoutHeaderGroup();

            showRoof = EditorGUILayout.BeginFoldoutHeaderGroup(showRoof, "Roof Layout");
            if (showFacade)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Roof material");
                buildingManager.RoofMaterial = (Material)EditorGUILayout.ObjectField(buildingManager.RoofMaterial, typeof(Material), true);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Roof material scale");
                buildingManager.RoofMaterialScale = EditorGUILayout.Slider(buildingManager.RoofMaterialScale, 0.1f, 5);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Roof slant distance");
                buildingManager.RoofSlantDistance = Mathf.Round(EditorGUILayout.Slider(buildingManager.RoofSlantDistance, 0, 5f) * 10f) / 10f;
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Roof overhang distance");
                buildingManager.RoofOverhangDistance = Mathf.Round(EditorGUILayout.Slider(buildingManager.RoofOverhangDistance, 0.1f, 2f) * 20f) / 20f;
                EditorGUILayout.EndHorizontal();

                buildingManager.RenderRoofGizmos = EditorGUILayout.ToggleLeft("Roof Gizmos", buildingManager.RenderRoofGizmos);
            }
            EditorGUILayout.EndFoldoutHeaderGroup();

            showDetails = EditorGUILayout.BeginFoldoutHeaderGroup(showDetails, "Details");
            if (showFacade)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Details material");
                buildingManager.DetailsManager.DetailsTrimMaterial = (Material)EditorGUILayout.ObjectField(buildingManager.DetailsManager.DetailsTrimMaterial, typeof(Material), true);
                EditorGUILayout.EndHorizontal();

                // Windows

                //EditorGUILayout.Space();
                //EditorGUILayout.LabelField("Windows", section2/*, GUILayout.Height(20)*/);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Material scale");
                buildingManager.DetailsManager.MaterialScale = EditorGUILayout.Slider(buildingManager.DetailsManager.MaterialScale, 0.1f, 1f);

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Facade Frame Trim Position");
                buildingManager.DetailsManager.FacadeFrameTrimPos = EditorGUILayout.Slider(buildingManager.DetailsManager.FacadeFrameTrimPos, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Facade Frame Trim Height");
                buildingManager.DetailsManager.FacadeFrameTrimHeight = EditorGUILayout.Slider(buildingManager.DetailsManager.FacadeFrameTrimHeight, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Glass Trim Position");
                buildingManager.DetailsManager.GlassTrimPos = EditorGUILayout.Slider(buildingManager.DetailsManager.GlassTrimPos, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Glass Trim Height");
                buildingManager.DetailsManager.GlassTrimHeight = EditorGUILayout.Slider(buildingManager.DetailsManager.GlassTrimHeight, 0f, 1f);
                EditorGUILayout.EndHorizontal();


                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Lining width");
                buildingManager.DetailsManager.LiningWidth = EditorGUILayout.Slider(buildingManager.DetailsManager.LiningWidth, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Frame thickness");
                buildingManager.DetailsManager.FrameThickness = EditorGUILayout.Slider(buildingManager.DetailsManager.FrameThickness, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Edge height");
                buildingManager.DetailsManager.EdgeHeight = EditorGUILayout.Slider(buildingManager.DetailsManager.EdgeHeight, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Glass thickness");
                buildingManager.DetailsManager.GlassThickness = EditorGUILayout.Slider(buildingManager.DetailsManager.GlassThickness, .01f, 0.2f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Glass position offset");
                buildingManager.DetailsManager.GlassPositionOffset = EditorGUILayout.Slider(buildingManager.DetailsManager.GlassPositionOffset, -0.5f, 0.5f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Door panel thickness");
                buildingManager.DetailsManager.DoorPanelThickness = EditorGUILayout.Slider(buildingManager.DetailsManager.DoorPanelThickness, 0.05f, 0.5f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Door panel position offset");
                buildingManager.DetailsManager.DoorPanelPosOffset = EditorGUILayout.Slider(buildingManager.DetailsManager.DoorPanelPosOffset, -.5f, .5f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Door panel Trim Position");
                buildingManager.DetailsManager.DoorPanelTrimPos = EditorGUILayout.Slider(buildingManager.DetailsManager.DoorPanelTrimPos, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Door panel Trim Height");
                buildingManager.DetailsManager.DoorPanelTrimHeight = EditorGUILayout.Slider(buildingManager.DetailsManager.DoorPanelTrimHeight, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Door panel Material angle");
                buildingManager.DetailsManager.DoorPanelMaterialAngle = EditorGUILayout.Slider(buildingManager.DetailsManager.DoorPanelMaterialAngle, 0f, 90f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Door panel Material scale");
                buildingManager.DetailsManager.DoorPanelMaterialScale = EditorGUILayout.Slider(buildingManager.DetailsManager.DoorPanelMaterialScale, 0f, 1f);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();

                buildingManager.DetailsManager.RenderDetailGizmos = EditorGUILayout.ToggleLeft("Detail Gizmos", buildingManager.DetailsManager.RenderDetailGizmos);
            }
            EditorGUILayout.EndFoldoutHeaderGroup();
        }





        serializedObject.ApplyModifiedProperties();

        if (EditorGUI.EndChangeCheck())
        {
            buildingManager.Generate();
            EditorUtility.SetDirty(buildingManager);
        }




        GUILayout.Space(10);


        if (GUILayout.Button("Generate"))
        {
            buildingManager.Generate();
        }
    }
}
