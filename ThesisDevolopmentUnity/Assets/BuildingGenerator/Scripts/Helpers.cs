﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public static class Helpers
{
    public static Vertex2D ToVertex2D(this Vector2 v)
    {
        return new Vertex2D(v);
    }

    public static Vertex2D[] ToVertex2DArray(this Vector2[] vector2Array)
    {
        List<Vertex2D> vertices = new List<Vertex2D>();
        foreach (Vector2 vector in vector2Array)
        {
            vertices.Add(new Vertex2D(vector));
        }
        return vertices.ToArray();
    }
    
    public static Vertex2D[] ToVertex2DArray(this List<Vector2> vector2List)
    {
        return vector2List.ToArray().ToVertex2DArray();
    }

    public static float RealAngleTo(this Vector2 v1, Vector2 v2)
    {

        float dot = v1.x * v2.x + v1.y * v2.y; //     # dot product between [x1, y1] and [x2, y2]
        float det = v1.x * v2.y - v1.y * v2.x; //      # determinant
            

        return Mathf.Atan2(det, dot);
    }

    public static Vector2 RotateRadians(this Vector2 v, float radians)
    {
        float xPrim = v.x * Mathf.Cos(radians) - v.y * Mathf.Sin(radians);
        float yPrim = v.x * Mathf.Sin(radians) + v.y * Mathf.Cos(radians);
        return new Vector2(xPrim, yPrim);
    }

    public static Vector2? ClosestTo(this Vector2 v, Vector2[] vectors, bool ignoreSelf)
    {
        if (vectors == null)
            return null;

        float closest = float.MaxValue;
        Vector2? c = null;
        foreach (Vector2 other in vectors)
        {
            if (v != other)
            {
                float dist = Vector2.Distance(v, other);
                if (dist < closest)
                {
                    closest = dist;
                    c = other;
                }
            }
        }

        return c;
    }


    public static int[] ToIntArray(this Triangle[] triangles, int addIndex)
    {
        List<int> tris = new List<int>();

        foreach (Triangle triangle in triangles)
        {
            tris.Add(triangle.v0 + addIndex);
            tris.Add(triangle.v1 + addIndex);
            tris.Add(triangle.v2 + addIndex);

        }
        return tris.ToArray();
    }

    public static int[] ToIntArray(this Triangle[] triangles)
    {

        return triangles.ToIntArray(0);
    }

    public static int[] ToIntArray(this List<Triangle> triangles, int addIndex)
    {

        return triangles.ToArray().ToIntArray(addIndex);
    }

    public static int[] ToIntArray(this List<Triangle> triangles)
    {
        
        return triangles.ToArray().ToIntArray();
    }

    public static bool Approximately(float a, float b, float epsilon)
    {
        return Mathf.Abs(a - b) <= epsilon;
    }

}

