﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEditorInternal;
using System;

[CustomEditor(typeof(OffsetTest))]
public class OffsetTestEditor : Editor
{
    private OffsetTest offsetTest;
    //private ReorderableList points;

    void OnEnable()
    {
        offsetTest = (OffsetTest)target;
        //points = new ReorderableList(serializedObject, serializedObject.FindProperty("points"), true, true, true, true);
        //points.drawHeaderCallback += DrawHeader;
        //points.drawElementCallback += DrawElement;
    }

    public override void OnInspectorGUI()
    {
        
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();
        if (EditorGUI.EndChangeCheck())
        {
            offsetTest.Generate();
            EditorUtility.SetDirty(offsetTest);
        }

        if (GUILayout.Button("Generate"))
        {
            offsetTest.Generate();
        }
    }

    //private void DrawHeader(Rect rect)
    //{
    //    GUI.Label(rect, "Point list");
    //}

    //private void DrawElement(Rect rect, int index, bool isActive, bool isFocused)
    //{
    //    float height = EditorGUIUtility.singleLineHeight;

    //    SerializedProperty element = points.serializedProperty.GetArrayElementAtIndex(index);

    //    SerializedProperty transform = element.FindPropertyRelative("angle");

    //    SerializedProperty edgeIndex = element.FindPropertyRelative("edgeIndex");
    //    SerializedProperty edgeDist = element.FindPropertyRelative("edgeDist");



    //    EditorGUI.LabelField(new Rect(rect.x, rect.y, 75, height), "Edge index");
    //    EditorGUI.PropertyField(new Rect(rect.x + 75, rect.y, 50, height), edgeIndex, GUIContent.none);

    //    EditorGUI.LabelField(new Rect(rect.x + 150, rect.y, 75, height), "Edge dist.");
    //    EditorGUI.PropertyField(new Rect(rect.x + 215, rect.y, 50, height), edgeDist, GUIContent.none);

    //    EditorGUI.LabelField(new Rect(rect.x + 290, rect.y, 50, height), "Angle");
    //    EditorGUI.PropertyField(new Rect(rect.x + 335, rect.y, 50, height), angle, GUIContent.none);


    //}

    //public override void OnInspectorGUI()
    //{
    //    EditorGUI.BeginChangeCheck();


    //    //EditorGUILayout.BeginHorizontal();
    //    //EditorGUILayout.LabelField("point list");
    //    ////var prop = serializedObject.FindProperty("points");
    //    //if (offsetTest.Points == null)
    //    //    offsetTest.Points = new Transform[0];
    //    //for (int i = 0; i < offsetTest.Points.Length; i++)
    //    //{
    //    //    EditorGUILayout.BeginHorizontal();
    //    //    EditorGUILayout.LabelField("point " + i);
    //    //    offsetTest.Points[i] = (Transform)EditorGUILayout.ObjectField(offsetTest.Points[i], typeof(Transform), true);
    //    //    EditorGUILayout.EndHorizontal();
    //    //}

    //    //offsetTest.Points = (Transform[])EditorGUILayout.ObjectField(offsetTest.Points, typeof(Transform[]), true);
    //    //EditorGUILayout.EndHorizontal();


    //    if (EditorGUI.EndChangeCheck())
    //    {
    //        offsetTest.Generate();
    //        EditorUtility.SetDirty(offsetTest);
    //    }

    //    if (GUILayout.Button("Generate"))
    //    {
    //        offsetTest.Generate();
    //    }
    //}
}
