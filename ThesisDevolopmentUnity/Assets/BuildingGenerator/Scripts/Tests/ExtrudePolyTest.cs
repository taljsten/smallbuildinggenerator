﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtrudePolyTest : MonoBehaviour
{
    //[SerializeField] private Vector2[] points;
    [SerializeField] private float radius = 2;
    [SerializeField] private int num = 6;
    [SerializeField] float extrudedistance = 5;
    private Polygon2D polygon;
    private MeshData meshData;
    private Mesh mesh;
    // Start is called before the first frame update
    void Start()
    {
        polygon = new RegularPolygon2D(EdgeType.External, radius, num);
        

        meshData = polygon.Extrude(extrudedistance, false);
        mesh = new Mesh();
        mesh.vertices = meshData.Vertices;
        mesh.triangles = meshData.Triangles.ToIntArray();
        mesh.uv = meshData.UV;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.RecalculateTangents();
        gameObject.GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    void OnDrawGizmos()
    {
        if (polygon != null)
            meshData.DrawGizmos();
    }
}
