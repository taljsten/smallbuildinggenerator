﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetTest : MonoBehaviour
{
    private Polygon2D polygon;
    private Polygon2D offset;
    //public Polygon2D Polygon { get => polygon; set => polygon = value; }
    [SerializeField] private Vector2[] points;
    //public Vector2[] Points { get => points; set => points = value; }
    [SerializeField] private float offsetDistance;
    [SerializeField] private OffsetMethod method;

    public void Generate()
    {
        polygon = new Polygon2D(EdgeType.External);
        foreach (Vector2 point in points)
        {
            polygon.Add(new Vertex2D(point));
        }
        polygon.Close();

        offset = polygon.Offset(offsetDistance, method);

        //Debug.Log("TEST");
    }

    void OnDrawGizmos()
    {
        if (polygon != null)
        {
            polygon.DrawGizmos(0f);
        }

        if (offset != null)
        {
            offset.DrawGizmos(0f);
        }
    }
}
