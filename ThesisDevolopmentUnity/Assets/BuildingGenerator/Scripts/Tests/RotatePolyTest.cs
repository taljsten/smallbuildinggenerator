﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePolyTest : MonoBehaviour
{
    [SerializeField] private Vector2[] polyPoints;
    [SerializeField] private float angle = 30;
    private Polygon2D poly;
    // Start is called before the first frame update
    void Start()
    {
        poly = new Polygon2D(EdgeType.External, polyPoints, true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            poly = poly.Rotate(angle * Mathf.Deg2Rad);
        }
    }

    void OnDrawGizmos()
    {
        if (poly != null)
            poly.DrawGizmos(0f);
    }
}
