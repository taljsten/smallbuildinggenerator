﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMesh 
{
    protected List<Vector3> vertices;
    protected List<int> triangles;
    private Vector2[] uvs;
    public Mesh Mesh { get; protected set; }
   

    public BaseMesh() 
    {
        Mesh = new Mesh();
        CreateMesh();
        Recalculate();
    }   

    protected virtual void CreateMesh()
    {
        vertices = new List<Vector3>();
        
        vertices.Add(new Vector3(-.5f, 0, -.5f));
        vertices.Add(new Vector3(.5f, 0, -.5f));
        vertices.Add(new Vector3(-.5f, 0, .5f));
        vertices.Add(new Vector3(.5f, 0, .5f));

       
        triangles = new List<int>();
        triangles.Add(0);
        triangles.Add(2);
        triangles.Add(3);
        triangles.Add(0);
        triangles.Add(3);
        triangles.Add(1);
        Mesh.vertices = vertices.ToArray();
        Mesh.triangles = triangles.ToArray();

    }

    protected virtual void Recalculate()
    {
        Mesh.RecalculateNormals();
        Mesh.RecalculateBounds();
    }
}
