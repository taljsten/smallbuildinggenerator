﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiMesh 
{
    private List<BaseMesh> subMeshes;
	public Mesh Mesh { get; private set; }

	public MultiMesh()
	{
		subMeshes = new List<BaseMesh>();
		Mesh = new Mesh();
	}

	public void Add(BaseMesh baseMesh)
	{
		subMeshes.Add(baseMesh);

		CombineInstance[] combines = new CombineInstance[subMeshes.Count];
		for (int i = 0; i < combines.Length; i++)
		{
			combines[i].mesh = subMeshes[i].Mesh;
		}

		Mesh.CombineMeshes(combines, true, false);

	}


}
