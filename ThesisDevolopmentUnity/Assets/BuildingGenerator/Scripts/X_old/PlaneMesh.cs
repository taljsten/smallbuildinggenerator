﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneMesh : BaseMesh
{
    Vector3[] points = new Vector3[4];
    

    public PlaneMesh(Vector3[] points)
    {
        this.points = points;
        if (points != null && points.Length == 4)
        {
            Mesh = new Mesh();
            CreateMesh();
            Recalculate(); 
        }
        
    }

    protected override void CreateMesh()
    {
        List<Vector3> verts = new List<Vector3>();

        foreach (Vector3 point in points)
        {
            Debug.Log(point);
            verts.Add(point);
        }

        List<int> tris = new List<int>();
        tris.Add(0);
        tris.Add(2);
        tris.Add(3);
        tris.Add(0);
        tris.Add(3);
        tris.Add(1);

        Mesh.vertices = verts.ToArray();
        Mesh.triangles = tris.ToArray();
    }

    protected override void Recalculate()
    {
        base.Recalculate();
    }
}

