﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RegularPolyTest))]
public class RegularPolyTestEditor : Editor
{
    private RegularPolyTest regularPolyTest;

    private void OnEnable()
    {
        regularPolyTest = (RegularPolyTest)target;
    }


    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();
        if (EditorGUI.EndChangeCheck())
        {
            regularPolyTest.Execute();
        }
    }
}
