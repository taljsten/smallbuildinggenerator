﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WindowTest))]
public class WindowTestEditor : Editor
{
    private WindowTest windowTest;

    private void OnEnable()
    {
        windowTest = (WindowTest)target;
    }


    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();
        if (EditorGUI.EndChangeCheck())
        {
           // windowTest.Execute();
        }
    }
}
