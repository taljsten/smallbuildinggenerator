﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
[ExecuteInEditMode]
public class House : MonoBehaviour
{

    private Floor floor; // Test
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    //[SerializeField] private Transform[] basePoints;
    [SerializeField] private Material interiorMaterial;
    [SerializeField,Range(1, 100)] private float texelResolution = 5;
    [SerializeField] private float roomLength = 5;
    [SerializeField] private float roomWidth = 5;
    [SerializeField] private float stripPos = 0;
    [SerializeField] private float stripHeight = 0.25f;
    private GameObject houseRoot;

    public float TexelResolution { get => texelResolution; set => texelResolution = value; }
    public float RoomLength { get => roomLength; set => roomLength = value; }
    public float RoomWidth { get => roomWidth; set => roomWidth = value; }
    public float StripPos { get => stripPos; set => stripPos = value; }
    public Material InteriorMaterial { get => interiorMaterial; set => interiorMaterial = value; }

    // Start is called before the first frame update
    void Start()
    {
        //Generate();
        
       
    }

    public void Generate()
    {

        RemoveOldHouse();
        CreateRoot();
        GetMeshComponents();

        //basePoints = new Transform[4];
        //for (int i = 0; i < 4; i++)
        //{
        //    GameObject g = new GameObject();

        //}

        //List<Vector3> points = new List<Vector3>();
        //foreach (Transform basePoint in basePoints)
        //{
        //    points.Add(basePoint.position);
        //}

        Vector3[] vrts = {
        new Vector3(0,0,0),
        new Vector3(roomLength,0,0),
        new Vector3(0,0,roomWidth),
        new Vector3(roomLength,0,roomWidth)
        };


        float length = Vector3.Distance(vrts[2], vrts[3]);
        float height = Vector3.Distance(vrts[0], vrts[2]);
 
        //float ratio = vrts[3].x / vrts[3].z;
        //float ratio = length / height;
        float v = stripPos + roomWidth * texelResolution / 100;
        float u = roomLength * texelResolution / 100;

        //Debug.Log(u + ", " + v);

        Vector2[] uvs = {
        new Vector2(0, stripPos),
        new Vector2(u, stripPos),
        new Vector2(0, v),
        new Vector2(u, v)

        };


        floor = new Floor(vrts, uvs);

        meshFilter.mesh = floor.Mesh;
        AssignMaterials();
    }

    private void AssignMaterials()
    {
        if (interiorMaterial)
        {
            meshRenderer.material = interiorMaterial;
        }
    }

    private void GetMeshComponents()
    {
        meshFilter = gameObject.GetComponent<MeshFilter>();
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
    }

    private void CreateRoot()
    {
        houseRoot = new GameObject("House Root");
        houseRoot.transform.parent = transform;
    }

    private void RemoveOldHouse()
    {
        if (houseRoot)
        {
            DestroyImmediate(houseRoot);
        }
    }

    void OnValidate()
    {
        //Generate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
