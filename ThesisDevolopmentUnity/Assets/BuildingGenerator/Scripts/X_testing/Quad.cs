﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quad 
{
    private Vector3[] vertices;
    private int[] triangles;
    private Vector2[] uvs;
    public Mesh Mesh { get; private set; }

    public Quad(Vector3[] verts, Vector2[] uvs)
    {
        
        vertices = verts;
        this.uvs = uvs;
        CreateTris();
        PopulateMesh();
    }

    protected virtual void PopulateMesh()
    {
        Mesh = new Mesh();
        Mesh.vertices = vertices;
        Mesh.triangles = triangles;
        Mesh.uv = uvs;
        Mesh.RecalculateNormals();
        Mesh.RecalculateBounds();
       
    }

    protected virtual void CreateTris()
    {
        // 0 --- 1
        // |  \  |
        // 2 --- 3

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 2;
        triangles[2] = 3;
        triangles[3] = 0;
        triangles[4] = 3;
        triangles[5] = 1;

    }
}
