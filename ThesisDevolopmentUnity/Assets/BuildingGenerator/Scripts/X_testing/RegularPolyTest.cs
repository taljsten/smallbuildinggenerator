﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularPolyTest : MonoBehaviour
{
    private RegularPolygon2D regularPolygon;
    [SerializeField] private float radius=7;
    [SerializeField] private int num = 5;
    [SerializeField] private Vector3 pos = Vector3.zero;
    [SerializeField] private Vector3 up = Vector3.up;
    [SerializeField] private Vector3 right = Vector3.right;




    // Start is called before the first frame update
    void Start()
    {
        Execute();

    }

    public void Execute()
    {
        regularPolygon = new RegularPolygon2D(EdgeType.External, radius, num);
        regularPolygon.Meshify(pos, up, right);
    }


    void OnDrawGizmos()
    {
        if (regularPolygon != null)
        {
           // regularPolygon.DrawGizmos(0f);
            regularPolygon.MeshData.DrawGizmos();
        }
    }

    
}
