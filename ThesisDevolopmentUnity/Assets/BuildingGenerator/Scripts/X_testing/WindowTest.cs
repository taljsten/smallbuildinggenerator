﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowTest : MonoBehaviour
{
    private Window window;
    [SerializeField] float liningWidth = 0.1f;
    [SerializeField] float frameThickness = .02f;
    [SerializeField] Vector3 pos;
    [SerializeField] Vector3 dir = Vector3.right;


    // Start is called before the first frame update
    void Start()
    {
        //Execute(detailsManager);
    }

    public void Execute(DetailsManager detailsManager)
    {
        //window = new Window(BuildRules.DetailsRuleSet, pos, dir, liningWidth, frameThickness, detailsManager);
    }

    // Update is called once per frame
    private void OnDrawGizmos()
    {
        if (window != null)
        {
            //window.Inner.DrawGizmos(0f);
            //window.Center.DrawGizmos(0f);
            //window.Outer.DrawGizmos(0f);
            foreach (MeshData meshData in window.MeshData)
            {
                meshData.DrawGizmos();
            }
        }
    }
}
