﻿using System.Numerics;

namespace GenerationTools.Snapper
{
    public struct Line
    {
        public Vector2 point1;
        public Vector2 point2;

        public Line(Vector2 p1, Vector2 p2)
        {
            this.point1 = p1;
            this.point2 = p2;
        }

        public Vector2? Intersection(Line other)
        {
            return null;        
        }

        public Vector2 Direction
        {
            get
            {               
                return Vector2.Normalize(point2 - point1);
            }
        }

        public float? GetYValue(float x)
        {
            if (point2.X - point1.X != 0)
            {
                float k = (point2.Y - point1.Y) / (point2.X - point1.X);
                float m = (point1.Y - k * point1.X);
                return k * x + m;
            }
            return null;
        }

        public override string ToString()
        {
            return "from: " + point1 + " to " + point2;
        }
    }
}
