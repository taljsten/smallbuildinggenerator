﻿
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace GenerationTools.Snapper
{
    public class Quadrilateral:Shape
    {
        public Quadrilateral(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3) : base()
        {
            vertices.Add(p0);
            vertices.Add(p1);
            vertices.Add(p2);
            vertices.Add(p3);
            edges.Add(new Line(p0, p1));
            edges.Add(new Line(p1, p2));
            edges.Add(new Line(p2, p3));
            edges.Add(new Line(p3, p0));
        }
    }
}
