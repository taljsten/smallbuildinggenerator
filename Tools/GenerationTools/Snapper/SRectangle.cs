﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace GenerationTools.Snapper
{
    public class SRectangle : Quadrilateral
    {
        public SRectangle(Vector2 pos, float width, float length) : base(pos, pos + new Vector2(0,length), pos + new Vector2(width,length), pos+new Vector2(width,0))
        {
        }
    }
}
