﻿using System;

namespace GenerationTools.Snapper
{
    public struct SVector2
    {
        public float x;
        public float y;

        public SVector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static SVector2 operator -(SVector2 v1, SVector2 v2)
        {
            return new SVector2(v1.x - v2.x, v1.y - v2.y); 
        }

        public static SVector2 operator +(SVector2 v1, SVector2 v2)
        {           
            return new SVector2(v1.x + v2.x, v1.y + v2.y);
        }

        public static SVector2 operator /(SVector2 v1, float divisor)
        {
            return new SVector2(v1.x / divisor, v1.y / divisor);
        }

        public static SVector2 operator /(SVector2 v1, SVector2 divisor)
        {
            return new SVector2(v1.x / divisor.x, v1.y / divisor.y);
        }

        public static SVector2 operator *(SVector2 v1, SVector2 m)
        {
            return new SVector2(v1.x * m.x, v1.y * m.y);
        }

        public static SVector2 operator *(SVector2 v1, float m)
        {
            return new SVector2(v1.x * m, v1.y * m);
        }

        public float Length
        {
            get
            {
                return (float)Math.Sqrt(x * x + y * y);
            }
        }

        public void Normalize()
        {
            float l = Length;
            this /= l;
        }

        public override string ToString()
        {
            return "[" + x + " : " + y + "]";
        }

        public SVector2 Normalized
        {
            get
            {
                float l = Length;
                return new SVector2(x / l, y / l);
            }
        }
    }
}
