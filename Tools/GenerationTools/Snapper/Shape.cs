﻿using System.Collections.Generic;
using System.Text;
using System.Numerics;

namespace GenerationTools.Snapper
{

    public class Shape
    {
        protected List<Vector2> vertices;
        protected List<Line> edges;
        

        public Shape()
        {
            vertices = new List<Vector2>();
            edges = new List<Line>();
            
        }

        public override string ToString()
        {
            string s = "Positions : \n";
            foreach (Vector2 v in vertices)
            {
                s += v.ToString() + "\n";
            }
            s += "Edges : \n";

            foreach (Line e in edges)
            {
                s += e.ToString() + "\n";
            }



            return s;
        }
    }
}
