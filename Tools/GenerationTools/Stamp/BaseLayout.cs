﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenerationTools.Stamp
{
    public class BaseLayout
    {
        private Shape startShape;
        private StampRule stampRule;

        public BaseLayout(Shape startShape, StampRule stampRule)
        {
            this.startShape = startShape;
            this.stampRule = stampRule;
        }

        public void Stamp(Shape stamp)
        {

        }

    }
}
