﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenerationTools.Stamp
{
    public class Rectangle : Shape
    {
        public Rectangle(int length, int width) : base(length, width)
        {


        }

        protected override void CreateShape()
        {
            for (int x = 0; x < SizeX; x++)
            {
                for (int y = 0; y < SizeY; y++)
                {
                    if (x == 0 || x == SizeX - 1 || y == 0 || y == SizeY - 1)
                        Set(x, y, Feature.Wall);
                    else
                        Set(x, y, Feature.Floor);

                }
            }
        }
    }
}
