﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenerationTools.Stamp
{
    public abstract class Shape
    {
        protected Feature[,] values;

        public int SizeX { get; protected set; }
        public int SizeY { get; protected set; }

        /// <summary>
        /// Creates a new shape array
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        /// <param name="fillWith"></param>
        protected Shape(int sizeX, int sizeY, Feature fillWith)
        {
            this.SizeX = sizeX;
            this.SizeY = sizeY;


            values = new Feature[sizeX, sizeY];
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    values[x, y] = fillWith;
                }
            }

            CreateShape();
        }


        /// <summary>
        /// Creates a new shape array
        /// </summary>
        /// <param name="sizeX"></param>
        /// <param name="sizeY"></param>
        protected Shape(int sizeX, int sizeY) : this(sizeX, sizeY, Feature.Void) { }

        protected abstract void CreateShape();

        //protected virtual void Stamp(int posX, int posY, Shape stamp, StampRule rule, bool extendShape)
        //{


        //}
        

        /// <summary>
        /// Gets a value from the shape array
        /// </summary>
        /// <param name="x">the horizontal pos in the array</param>
        /// <param name="y">the vertical pos in the array</param>
        /// <returns></returns>
        public virtual Feature Value(int x, int y)
        {
            return values[x, y];
        }

        /// <summary>
        /// Sets a value in the shape array
        /// </summary>
        /// <param name="x">the horizontal pos in the array</param>
        /// <param name="y">the vertical pos in the array</param>
        /// <param name="value"></param>
        /// <returns>true if position is valid</returns>
        public virtual bool Set(int x, int y, Feature value)
        {
            if (values == null)
                return false;
            if (x < 0 || x >= values.GetLength(0))
                return false;
            if (y < 0 || y >= values.GetLength(1))
                return false;

            values[x, y] = value;
            return true;
        }

        
    }
}
