﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenerationTools.Stamp
{
    public class StampRuleSimple : StampRule
    {
        public override Feature Stamp(Feature baseFeature, Feature stampFeature)
        {
            Feature newFeature = baseFeature;
            switch (baseFeature)
            {
                case Feature.Void:
                    newFeature = stampFeature;
                    break;
                case Feature.Floor:
                    newFeature = baseFeature;
                    break;
                case Feature.Wall:
                    switch (stampFeature)
                    {
                        case Feature.Void:
                            newFeature = baseFeature;
                            break;
                        case Feature.Floor:
                            newFeature = stampFeature;
                            break;
                        case Feature.Wall:
                            newFeature = stampFeature;
                            break;
                        default:
                            break;
                    }
                    break;
            }

            return newFeature;
        }
    }
}
