﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenerationTools.Stamp
{
    public abstract class StampRule
    {
        public abstract Feature Stamp(Feature baseFeature, Feature stampFeature);
    }
}
